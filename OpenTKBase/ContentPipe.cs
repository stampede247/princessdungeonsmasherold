﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using System.IO;
using OpenTK.Audio.OpenAL;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// Holds the functions for loading an image and for loading a sound file
    /// </summary>
    class ContentPipe
    {
        /// <summary>
        /// Loads a texture from a file with the specified filename. 
        /// Uses System.Drawing.Imaging to load the file and passes the data to OpenGL.
        /// Also supports built in animation if it finds a .ani file of the same filename.
        /// Ani file just contains a number representing the number of frames in the image
        /// Supports BMP, GIF, EXIF, JPG, PNG and TIFF (GIF animation not supported)
        /// 
        /// Throws: ArgumentException if file doesn't exist
        ///         FileLoadException if loading process encounters exception/error
        /// </summary>
        /// <param name="filename">"Content/Sprites/" is assumed and should not be included. File extension is required.</param>
        /// <param name="pixelated">if true it will set the texture sampling method to NearestNeighbor</param>
        /// <returns>Texture2D struct containing texture info.</returns>
        public static Texture2D LoadTexture(string filename, bool pixelated = true)
        {
            if (filename == "") //No filename
            {
                throw new ArgumentException("Empty file name.", "filename");
            }
            if (!Path.HasExtension(filename))//No extension
            {
                throw new ArgumentException("No extension: '" + filename + "'");
            }

            //Add the assumed directory
            string filePath = "Content/Sprites/" + filename;

            if (!System.IO.File.Exists(filePath))//Check if file exists
            {
                throw new ArgumentException("File could not be found at '" + Path.GetFullPath(filePath) + "'");
            }

            bool isAnimated = false;
            int numFrames = 1;
            //The ani file simply contains a number for how many frames are in the animation
            #region Check if animation strip
            try
            {
                string fileName = Path.GetFileNameWithoutExtension(filePath);
                string folder = Path.GetDirectoryName(filePath);
                string infoFilePath = folder + "/" + fileName + ".ani";
                if (File.Exists(infoFilePath))
                {
                    using (StreamReader reader = new StreamReader(infoFilePath))
                    {
                        if (!int.TryParse(reader.ReadLine(), out numFrames))
                        {
                            Console.WriteLine("Found ani file but couldn't read number. '" + infoFilePath + "'");
                            numFrames = 1;
                            isAnimated = false;
                        }
                        else
                        {
                            isAnimated = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Tried to load animation information but failed. Error: " + e.ToString());
            }
            #endregion

            try
            {
                //Ask OpenGL to create a new texture space for us and pass back the id
                //This id gets put into our Texture2D struct for reference later when drawing
                int id = GL.GenTexture();
                //Set the new texture as current so we can work on it
                GL.BindTexture(TextureTarget.Texture2D, id);

                //Use System.Drawing.Bitmap to load the image
                Bitmap bmp = new Bitmap(filePath);
                //Locks the color data bits in memory so we can stream them to OpenGL
                BitmapData bmpData = bmp.LockBits(
                    new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                //This is where the data is actually passed from the Bitmap to OpenGL under the current texture id
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                    bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                    PixelType.UnsignedByte, bmpData.Scan0);

                //Create Texture2D struct
                Texture2D tex;
                if (!isAnimated)
                    tex = new Texture2D(filename, id, bmp.Width, bmp.Height);
                else
                    tex = new Texture2D(filename, id, bmp.Width, bmp.Height, numFrames);

                //Let go of the data that was locked
                bmp.UnlockBits(bmpData);

                //We haven't unloaded mipmaps, so disavle mipmapping (otherwise the texture will not appear).
                //On newer video cards, we can use GL.GenerateMipmaps() or GL.Ext.GenerateMipmaps() to create
                //mipmaps automatically. In that case, use TextureMinFilter.LinearMipmapLinear to enable them.
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, pixelated ? (int)TextureMinFilter.Nearest : (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, pixelated ? (int)TextureMinFilter.Nearest : (int)TextureMinFilter.Linear);

                return tex;
            }
            catch (Exception e)
            {
                throw new FileLoadException("Could not successfully load file from '" + Path.GetFullPath(filePath) + "'.\nEncountered error: " + e.ToString());
            }
        }

        /// <summary>
        /// Loads a wav file using System.IO.BinaryReader and passes the data to OpenAL.
        /// Creates a SoundEffect struct with info about the file.
        /// Currently supports WAV files with 1 or 2 tracks and 8 or 16 bits per sample
        /// 
        /// Throws: NotSupportedException if WAVE file doesn't match criteria for standard wave files
        ///         ArgumentException if file doesn't exist or filename is invalid
        /// </summary>
        /// <param name="filename">"Content/" is assumed and should not be included. File extension is required</param>
        /// <returns>The SoundEffect struct that references the loaded sound</returns>
        public static SoundEffect LoadSoundEffect(string filename)
        {
            if (filename == "") //No filename
            {
                throw new ArgumentException("Empty file name.", "filename");
            }
            if (!Path.HasExtension(filename))//No extension
            {
                throw new ArgumentException("No extension: '" + filename + "'");
            }

            string filePath = "Content/" + filename;
            if (!File.Exists(filePath))// File doesn't exist
            {
                throw new ArgumentException("Could not find file '" + Path.GetFullPath(filePath) + "'");
            }

            SoundEffect effect;

            using (FileStream stream = new FileStream(filePath, FileMode.Open))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    //Contains the letters "RIFF" in ASCII form
                    //(0x52494646 big - endian form).
                    string signature = new string(reader.ReadChars(4));
                    if (signature != "RIFF")
                        throw new NotSupportedException("Specified stream is not a wave file");

                    //36 + SubChunk2Size, or more precisely:
                    //4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
                    //This is the size of the rest of the chunk following this number.
                    //This is the size of the entire file in bytes minus 8 bytes for the
                    //two fields not included in this count: ChunkID and ChunkSize
                    int riffChunkSize = reader.ReadInt32();

                    //Contains the letters "WAVE"
                    //(0x57415645 big - endian form).
                    string format = new string(reader.ReadChars(4));
                    if (format != "WAVE")
                        throw new NotSupportedException("Specified stream is not a wave file");

                    //Contains the letters "fmt "
                    //(0x666d7420 big - endian form)
                    string formatSignature = new string(reader.ReadChars(4));
                    if (formatSignature != "fmt ")
                        throw new NotSupportedException("Specified wave file is not supported");

                    //16 for PCM.  
                    //This is the size of the rest of the Subchunk which follows this number.
                    int formatChunkSize = reader.ReadInt32();

                    //PCM = 1 (i.e. Linear quantization)
                    //Values other than 1 indicate some form of compression.
                    int audioFormat = reader.ReadInt16();

                    //Mono = 1, Stereo = 2, etc.
                    int numChannels = reader.ReadInt16();

                    //8000, 44100, etc.
                    int sampleRate = reader.ReadInt32();

                    //== SampleRate * NumChannels * BitsPerSample/8
                    int byteRate = reader.ReadInt32();

                    //== NumChannels * BitsPerSample/8
                    //The number of bytes for one sample including all channels.
                    int blockAlign = reader.ReadInt16();

                    //8 bits = 8, 16 bits = 16, etc.
                    int bitsPerSample = reader.ReadInt16();

                    //Contains the letters "data"
                    //(0x64617461 big - endian form).
                    string dataSignature = new string(reader.ReadChars(4));
                    if (dataSignature != "data")
                        throw new NotSupportedException("Specified wave file is not supported");

                    //== NumSamples * NumChannels * BitsPerSample/8
                    //This is the number of bytes in the data.
                    int dataChunkSize = reader.ReadInt32();

                    //The actualy sound data
                    byte[] data = reader.ReadBytes(dataChunkSize);

                    int buffer = AL.GenBuffer();//Generate Buffer ID
                    int source = AL.GenSource();//Generate Source ID
                    
                    //This is where we actually pass the data to OpenGL
                    AL.BufferData(buffer, GetSoundFormat(numChannels, bitsPerSample), data, data.Length, sampleRate);
                    //Also create a source for the buffer
                    AL.Source(source, ALSourcei.Buffer, buffer);

                    //Fill the SoundEffect struct with all the info
                    effect = new SoundEffect(buffer, source, numChannels, bitsPerSample, sampleRate, dataChunkSize / blockAlign, (double)dataChunkSize / byteRate);

                }
            }//Close the file

            return effect;
        }

        /// <summary>
        /// Used to quickly get the ALFormat that coorisponds with # channels and bitsPerSample
        /// 
        /// Throws: NotSupportedException if channels > 2 or if bits is not 8 or 16
        /// </summary>
        /// <param name="channels">The number of channels in the wave audio</param>
        /// <param name="bits">The number of bits per sample in the audio</param>
        /// <returns>The coorisponding ALFormat</returns>
        private static ALFormat GetSoundFormat(int channels, int bits)
        {
            switch (channels)
            {
                case 1: return (bits == 8) ? ALFormat.Mono8 : ALFormat.Mono16;
                case 2: return (bits == 8) ? ALFormat.Stereo8 : ALFormat.Stereo16;
                default: throw new NotSupportedException("The specified sound format is not supported");
            };
        }
    }
}
