﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// A class to contain all my own public static functions that perform math-like functionality
    /// </summary>
    class Calc
    {
        /// <summary>
        /// Uses My.rand to generate 3 random values for RGB. (not A)
        /// </summary>
        /// <returns>A random color</returns>
        public static Color RandomColor()
        {
            int R = My.rand.Next(0, 256);
            int G = My.rand.Next(0, 256);
            int B = My.rand.Next(0, 256);
            return Color.FromArgb(R, G, B);
        }
        /// <summary>
        /// Acts like Lerp function for numbers but instead works on colors.
        /// </summary>
        /// <param name="c1">Color from</param>
        /// <param name="c2">Color to</param>
        /// <param name="amount">Sample position (0 = c1, 1 = c2)</param>
        /// <returns>A mixed color between c1 and c2</returns>
        public static Color Lerp(Color c1, Color c2, float amount)
        {
            return Lerp(c1, c2, (double)amount);
        }
        /// <summary>
        /// Acts like Lerp function for numbers but instead works on colors.
        /// </summary>
        /// <param name="c1">Color from</param>
        /// <param name="c2">Color to</param>
        /// <param name="amount">Sample position (0 = c1, 1 = c2)</param>
        /// <returns>A mixed color between c1 and c2</returns>
        public static Color Lerp(Color c1, Color c2, double amount)
        {
            return Color.FromArgb((int)Lerp(c1.A, c2.A, amount),
                (int)Lerp(c1.R, c2.R, amount),
                (int)Lerp(c1.G, c2.G, amount),
                (int)Lerp(c1.B, c2.B, amount));
        }

        /// <summary>
        /// Rotates the Point Clockwise around the Origin point 
        /// Only clockwise if y+ is down and x+ is right
        /// </summary>
        /// <param name="point">Original point</param>
        /// <param name="origin">The center of rotation</param>
        /// <param name="radians">Amount of rotation</param>
        /// <returns>A point rotated around the origin</returns>
        public static Vector2 RotateAround(Vector2 point, Vector2 origin, float radians)
        {
            Vector2 p = point - origin;

            double rotation = DirectionTo(origin, point) + radians;
            double test = MathHelper.RadiansToDegrees(rotation);

            Vector2 dX = new Vector2((float)Math.Cos(radians), (float)Math.Sin(radians));
            Vector2 dY = new Vector2((float)Math.Cos(radians + MathHelper.PiOver2), (float)Math.Sin(radians + MathHelper.PiOver2));

            p = (p.X * dX) + (p.Y * dY);

            p += origin;

            return p;
        }

        /// <summary>
        /// Used to find the direction between two Cartesian coordinates
        /// </summary>
        /// <param name="from">The position to start from</param>
        /// <param name="to">The position to point to</param>
        /// <returns> The angle from 'from' to 'to' in radians</returns>
        public static double DirectionTo(Vector2 from, Vector2 to)
        {
            double dir = Math.Atan2(to.Y - from.Y, to.X - from.X);
            return (dir < 0) ? (dir + MathHelper.Pi * 2) : (dir);
        }

        /// <summary>
        /// Finds the anle between two vectors. (Assuming they are at the same position)
        /// </summary>
        /// <param name="v1">The direction of vector1</param>
        /// <param name="v2">The direction of vector2</param>
        /// <returns>Angle, in radians, between vector1 and vector2</returns>
        public static double AngleBetween(Vector2 v1, Vector2 v2)
        {
            return Math.Acos(Vector2.Dot(v1, v2) / v1.Length / v2.Length);
        }

        /// <summary>
        /// Returns the distance between the two points
        /// </summary>
        public static float Distance(Vector2 p1, Vector2 p2)
        {
            return (float)Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }
        /// <summary>
        /// Returns the distance squared between the two points
        /// Useful when efficiency is important
        /// </summary>
        public static float DistanceSquared(Vector2 p1, Vector2 p2)
        {
            return (float)(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }

        /// <summary>
        /// Returns a point on the circle in the direction 'rotation' with radius
        /// </summary>
        /// <param name="direction">Angle from the center of the circle in Radians</param>
        /// <param name="radius">Radius of the circle</param>
        /// <returns>A point on a origin centered circle with a specified radius</returns>
        public static Vector2 CirclePoint(float direction, float radius)
        {
            return new Vector2(radius * (float)Math.Cos(direction), radius * (float)Math.Sin(direction));
        }

        /// <summary>
        /// Finds the rotational gap between two angles
        /// Solves the 0 = 360 degree problem
        /// e.g. 1 degree and 359 degrees will return 2 degrees
        /// </summary>
        /// <param name="angle1">Angle 1 in radians or Degrees (depending on 'inRadians')</param>
        /// <param name="angle2">Angle 2 in radians or Degrees (depending on 'inRadians')</param>
        /// <param name="inRadians">Whether or not to work in radians (inputs and output)</param>
        /// <returns>Difference from angle1 to angle2 (Not absolute value, can be negative)</returns>
        public static float AngleDifference(float angle1, float angle2, bool inRadians = true)
        {
            float full = (inRadians ? MathHelper.Pi * 2 : 360);
            float a1 = angle1;
            float a2 = angle2;
            if (a1 > full)
                a1 -= full;
            if (a1 < 0)
                a1 += full;
            if (a2 > full)
                a2 -= full;
            if (a2 < 0)
                a2 += full;

            if (Math.Abs(a1 - a2) > full / 2f)
            {
                a2 -= full;
            }

            return a2 - a1;
        }

        /// <summary>
        /// Simple helper to return whether value falls inside bounds
        /// </summary>
        /// <param name="p1">The first point. Can be max OR min point</param>
        /// <param name="p2">The second point. Can be max OR min point</param>
        /// <param name="value">The value to be checked</param>
        /// <param name="inclusive">Whether p1 == value or p2 == value are true cases</param>
        /// <returns>Whether or not 'value' falls between 'p1' and 'p2'</returns>
        public static bool WithinBounds(float p1, float p2, float value, bool inclusive = true)
        {
            if (inclusive)
                return (Math.Min(p1, p2) <= value && value <= Math.Max(p1, p2));
            else
                return (Math.Min(p1, p2) < value && value < Math.Max(p1, p2));
        }

        /// <summary>
        /// Helper function to determine if two "bounded regions" on an axis intersect each other
        /// </summary>
        /// <param name="p1">1st point of region 1</param>
        /// <param name="p2">2nd point of region 1</param>
        /// <param name="p3">1st point of region 2</param>
        /// <param name="p4">2nd point of region 2</param>
        /// <param name="inclusive">Whether or not same ending points cause a true case</param>
        /// <returns>Whether or not the two regions intersect</returns>
        public static bool BoundsIntersect(float p1, float p2, float p3, float p4, bool inclusive = true)
        {
            float min1 = Math.Min(p1, p2);
            float max1 = Math.Max(p1, p2);
            float min2 = Math.Min(p3, p4);
            float max2 = Math.Max(p3, p4);

            if (inclusive)
            {
                return (min1 <= max2 && max1 >= min2);
            }
            else
            {
                return (min1 < max2 && max1 > min2);
            }
        }

        /// <summary>
        /// Finds a value a specified amount between two values
        /// </summary>
        /// <param name="min">the minimum value</param>
        /// <param name="max">the maximum value</param>
        /// <param name="amount">the position to sample (0 = min value, 1 = max value)</param>
        /// <returns>A value between min and max</returns>
        public static float Lerp(float min, float max, double amount)
        {
            return min + (max - min) * (float)amount;
        }
        /// <summary>
        /// Finds a value a specified amount between two values
        /// </summary>
        /// <param name="min">the minimum value</param>
        /// <param name="max">the maximum value</param>
        /// <param name="amount">the position to sample (0 = min value, 1 = max value)</param>
        /// <returns>A value between min and max</returns>
        public static float Lerp(float min, float max, float amount)
        {
            return min + (max - min) * amount;
        }

        #region Longer Functions


        /// <summary>
        /// Does a continuous collision check between two Rectangles (one of them moving, other static)
        /// </summary>
        /// <param name="staticRec">The static rectangle</param>
        /// <param name="moveRec">The moving rectangle (with 'velocity')</param>
        /// <param name="velocity">Velocity of the moving rectangle</param>
        /// <param name="solution">The rectangle representing where moveRec should be at the end of the step</param>
        /// <param name="isPlatform">True = only collide with top of statRec</param>
        /// <returns>whether they collided</returns>
        public static bool SweepTest(RectangleF staticRec, RectangleF moveRec, Vector2 velocity, out RectangleF solution, bool isPlatform = false)
        {
            RectangleF startRec = moveRec;
            RectangleF endRec = new RectangleF(moveRec.X + velocity.X, moveRec.Y + velocity.Y, moveRec.Width, moveRec.Height);
            RectangleF unionRec = RectangleF.Union(startRec, endRec);

            if (!staticRec.IntersectsWith(unionRec))
            {
                // Static rectangle was nowhere inside the area we wanted to move through
                solution = endRec;
                return false;
            }

            #region JointX
            //displacement along the x-axis where the two rectangle will begin to be joined
            float jointX = 0;
            if (velocity.X > 0)
                jointX = staticRec.Left - startRec.Right;
            else
                //We make it negative because the displacement is in the -x direction
                jointX = -(startRec.Left - staticRec.Right);
            #endregion

            #region JointY
            //displacement along the y-axis where the two rectangle will begin to be joined
            float jointY = 0;
            if (velocity.Y > 0)
                jointY = staticRec.Top - startRec.Bottom;
            else
                //We make it negative because the displacement is in the -y direction
                jointY = -(startRec.Top - staticRec.Bottom);
            #endregion

            #region MaxJointTime
            //Find the 'times' at which the this joints occur
            //By time i mean 0-1 where 0 is begining of step and 1 is end of step with
            //full velocity added
            double jointTimeX = jointX / velocity.X;
            double jointTimeY = jointY / velocity.Y;

            if ((jointTimeX < 0 && jointTimeY < 0) || (jointTimeX > 1 && jointTimeY > 1))
            {
                //continuing this velocity will run into it eventually but not in this step
                //or the times are in the past (we are moving away from staticRec)
                solution = endRec;
                return false;
            }

            //This will hold the joint time that happens last/second
            //It will tell us the absolute latest they could be joined on both axises
            double maxJointTime;
            if (double.IsInfinity(jointTimeX) || double.IsInfinity(jointTimeY))
            {
                if (double.IsInfinity(jointTimeX) && !double.IsInfinity(jointTimeY))
                    maxJointTime = jointTimeY;
                else if (double.IsInfinity(jointTimeY) && !double.IsInfinity(jointTimeX))
                    maxJointTime = jointTimeX;
                else
                {
                    //Both joint times were infinite
                    //Usually happens if velocity was 0,0
                    solution = endRec;
                    return false;
                }
            }
            else
            {
                //Neither were infinite so take the greater one
                maxJointTime = Math.Max(jointTimeX, jointTimeY);
            }
            #endregion

            #region DisjointX
            //Now do the same process but for when they will 'disjoint'
            float disjointX = 0;
            if (velocity.X > 0)
                disjointX = staticRec.Right - startRec.Left;
            else
                disjointX = -(startRec.Right - staticRec.Left);
            #endregion

            #region DisjointY
            float disjointY = 0;
            if (velocity.Y > 0)
                disjointY = staticRec.Bottom - startRec.Top;
            else
                disjointY = -(startRec.Bottom - staticRec.Top);
            #endregion

            #region MinDisjointTime
            double disjointTimeX = disjointX / velocity.X;
            double disjointTimeY = disjointY / velocity.Y;

            //This time we want to know the earliest they will disjoint
            double minDisjointTime;
            if (double.IsInfinity(disjointTimeX) || double.IsInfinity(disjointTimeY))
            {
                if (double.IsInfinity(disjointTimeX) && !double.IsInfinity(disjointTimeY))
                    minDisjointTime = disjointTimeY;
                else if (double.IsInfinity(disjointTimeY) && !double.IsInfinity(disjointTimeX))
                    minDisjointTime = disjointTimeX;
                else
                {
                    //Both disjoint times were infinite
                    //Usually happens if velocity was 0,0
                    //We should never really hit this since it should 
                    //have been handled when going through jointTimes
                    solution = endRec;
                    return false;
                }
            }
            else
            {
                //Neither were infinite so take the lesser one
                minDisjointTime = Math.Min(disjointTimeX, disjointTimeY);
            }
            #endregion


            if (minDisjointTime < maxJointTime)
            {
                //One of the axises disjointed before both were joined
                //No overlap of joint periods from both axises
                solution = endRec;
                return false;
            }

            //We have a solution if we got here
            Vector2 solEndPosition = new Vector2(startRec.X, startRec.Y) + (velocity * (float)maxJointTime);

            if (isPlatform)
            {
                //We need to make sure it was a top collision
                #region Platform Handling
                if (jointTimeX > jointTimeY && !double.IsInfinity(jointTimeX))
                {
                    //We collided with either left or right face
                    solution = endRec;
                    return false;
                }

                if (jointTimeY > jointTimeX && !double.IsInfinity(jointTimeY) && velocity.Y < 0)
                {
                    //We collided with the bottom
                    solution = endRec;
                    return false;
                }

                if (double.IsInfinity(jointTimeY))
                {
                    solution = endRec;
                    return false;
                }
                if (double.IsInfinity(jointTimeX) && velocity.Y <= 0)
                {
                    solution = endRec;
                    return false;
                }
                #endregion
            }

            #region Rounding Solution
            //I added this so the edge values of the static and moving recs
            //will align if there was a collision
            //It helps when I want to know what blocks I am immediately next
            //to after the collision
            if (jointTimeX > jointTimeY && !double.IsInfinity(jointTimeX))
            {
                //Collision was in the x direction
                if (velocity.X > 0)
                    solEndPosition.X = staticRec.Left - startRec.Width;
                else if (velocity.X < 0)
                    solEndPosition.X = staticRec.Right;
            }
            if (jointTimeY > jointTimeX && !double.IsInfinity(jointTimeY))
            {
                //Collision was in the y direction
                if (velocity.Y > 0)
                    solEndPosition.Y = staticRec.Top - startRec.Height;
                else if (velocity.Y < 0)
                    solEndPosition.Y = staticRec.Bottom;
            }
            #endregion

            solution = new RectangleF(solEndPosition.X, solEndPosition.Y, startRec.Width, startRec.Height);
            return true;
        }

        /// <summary>
        /// Tests for intersection between two lines
        /// </summary>
        /// <param name="line1">The first line</param>
        /// <param name="line2">The second line</param>
        /// <param name="intersection">Point where the two lines intersect. Vector2.Zero if no intersection</param>
        /// <param name="inclusive">Whether or not two overlapping points are a true case</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVLine(Line line1, Line line2, out Vector2 intersection, bool inclusive = true)
        {
            return LineVLine(line1.p1, line1.p2, line2.p1, line2.p2, out intersection, inclusive);
        }
        /// <summary>
        /// Tests for intersection between two lines
        /// </summary>
        /// <param name="p1">First point of line 1</param>
        /// <param name="p2">Second point of line 1</param>
        /// <param name="p3">First point of line 2</param>
        /// <param name="p4">Second point of line 2</param>
        /// <param name="intersection">Point where the two lines intersect. Vector2.Zero if no intersection</param>
        /// <param name="inclusive">Whether or not two overlapping points are a true case</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVLine(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, out Vector2 intersection, bool inclusive = true)
        {
            #region Old
            //if (l1P1.X != l1P2.X)
            //{
            //    //This is the formula this is based off of
            //    //E = B-A = ( Bx-Ax, By-Ay )
            //    //F = D-C = ( Dx-Cx, Dy-Cy ) 
            //    //P = ( -Ey, Ex )
            //    //h = ( (A-C) * P ) / ( F * P )

            //    Vector2 A = l1P1;
            //    Vector2 B = l1P2;
            //    Vector2 C = l2P1;
            //    Vector2 D = l2P2;
            //    Vector2 E = B - A;
            //    Vector2 F = D - C;
            //    Vector2 P = new Vector2(-E.Y, E.X);
            //    float H = Vector2.Dot((A - C), P) / Vector2.Dot(F, P);
            //    if (0 <= H && H <= 1)
            //    {
            //        Vector2 ret = (l2P1 + (l2P2 - l2P1) * H);
            //        float H2 = (ret.X - l1P1.X) / (l1P2.X - l1P1.X);
            //        if (0 <= H2 && H2 <= 1)
            //        {
            //            intersection = ret;
            //            return true;
            //        }
            //    }
            //}
            //else
            //{
            //    //This is a handler for when the slope of l1 is undefined
            //    //I can't remember what this if was for but removing it fixed some problems..
            //    if (l2P1.X == l2P2.X)
            //    {
            //        //Parrallell lines 
            //        if (l2P1.X == l1P1.X && BoundsIntersect(l2P1.Y, l2P2.Y, l1P1.Y, l1P2.Y, true))
            //        {
            //            if (WithinBounds(l1P1.Y, l1P2.Y, l2P1.Y))
            //            {
            //                intersection = l2P1;
            //            }
            //            else if (WithinBounds(l1P1.Y, l1P2.Y, l2P2.Y))
            //            {
            //                intersection = l2P2;
            //            }
            //            else
            //            {
            //                intersection = (l1P1 + l1P2) / 2f;
            //            }
            //            return true;
            //        }
            //    }
            //    if (WithinBounds(l2P1.X, l2P2.X, l1P1.X))
            //    {
            //        float height = ((float)(l2P2.Y - l2P1.Y) / (float)(l2P2.X - l2P1.X)) * (l1P1.X - l2P1.X);
            //        height += l2P1.Y;
            //        if (l1P1.Y <= height && l1P2.Y >= height)
            //        {
            //            intersection = new Vector2(l1P1.X, height);
            //            return true;
            //        }
            //        else if (l1P1.Y >= height && l1P2.Y <= height)
            //        {
            //            intersection = new Vector2(l1P1.X, height);
            //            return true;
            //        }
            //    }
            //}
            //intersection = Vector2.Zero;
            //return false;
            #endregion

            if ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X) == 0)
            {
                //Lines are parrallel
                intersection = Vector2.Zero;
                return false;
            }

            intersection = Vector2.Zero;
            intersection.X =
                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X));
            intersection.Y =
                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X));

            Range r1 = new Range(p1.X, p2.X);
            Range r2 = new Range(p3.X, p4.X);
            //if (inclusive)
            //{
            //    r1.min -= ROUNDING_CUTOFF;
            //    r2.min -= ROUNDING_CUTOFF;
            //    r1.max += ROUNDING_CUTOFF;
            //    r2.max += ROUNDING_CUTOFF;
            //}
            if (Range.IsInside(r1, intersection.X, inclusive) &&
                Range.IsInside(r2, intersection.X, inclusive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Tests for intersection between a line and a rectangle (Does not account for 2 intersections case)
        /// </summary>
        /// <param name="rec">The rectangle to test against</param>
        /// <param name="line">The line to test</param>
        /// <param name="intersection1">Point where the two intersect. Vector2.Zero if no intersection</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVRectangle(OBB rec, Line line, out Vector2 intersection1)
        {
            Vector2? other;
            return LineVRectangle(rec, line.p1, line.p2, out intersection1, out other);
        }
        /// <summary>
        /// Tests for intersection between a line and a rectangle (Does not account for 2 intersections case)
        /// </summary>
        /// <param name="rec">The rectangle to test against</param>
        /// <param name="l1P1">First point of line 2</param>
        /// <param name="l1P2">Second point of line 2</param>
        /// <param name="intersection1">Point where the two intersect. Vector2.Zero if no intersection</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVRectangle(OBB rec, Vector2 l1P1, Vector2 l1P2, out Vector2 intersection1)
        {
            Vector2? other;
            return LineVRectangle(rec, l1P1, l1P2, out intersection1, out other);
        }
        /// <summary>
        /// Tests for intersection between a line and a rectangle
        /// </summary>
        /// <param name="rec">The rectangle to test against</param>
        /// <param name="line">The line to test</param>
        /// <param name="intersection1">First point where the two intersect. Vector2.Zero if no intersection</param>
        /// <param name="intersection2">Second point where the two intersect. null if no second intersection</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVRectangle(OBB rec, Line line, out Vector2 intersection1, out Vector2? intersection2)
        {
            return LineVRectangle(rec, line.p1, line.p2, out intersection1, out intersection2);
        }
        /// <summary>
        /// Tests for intersection between a line and a rectangle
        /// </summary>
        /// <param name="rec">The rectangle to test against</param>
        /// <param name="l1P1">The first point of the line to test</param>
        /// <param name="l1P2">The second point of the line to test</param>
        /// <param name="intersection1">First point where the two intersect. Vector2.Zero if no intersection</param>
        /// <param name="intersection2">Second point where the two intersect. null if no second intersection</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVRectangle(OBB rec, Vector2 l1P1, Vector2 l1P2, out Vector2 intersection1, out Vector2? intersection2)
        {
            Vector2[] corners = new Vector2[4]
            {
                rec.TopLeft,
                rec.TopRight,
                rec.BottomRight,
                rec.BottomLeft,
            };

            bool foundOne = false;
            intersection1 = Vector2.Zero;
            intersection2 = null;
            for (int i = 0; i < 4; i++)
            {
                Vector2 newInt;
                if (Calc.LineVLine(new Line(l1P1, l1P2), new Line(corners[i], corners[(i + 1) % 4]), out newInt))
                {
                    if (!foundOne)
                    {
                        intersection1 = newInt;
                        foundOne = true;
                    }
                    else
                    {
                        intersection2 = newInt;
                        break;
                    }
                }
            }
            if (intersection2.HasValue)
            {
                Vector2 i2 = intersection2.Value;
                float d1 = Calc.DistanceSquared(intersection1, l1P1);
                float d2 = Calc.DistanceSquared(intersection2.Value, l1P1);
                if (d1 > d2)
                {
                    intersection2 = intersection1;
                    intersection1 = i2;
                }
            }
            return foundOne;
        }
        /// <summary>
        /// Tests for intersection between a line and a rectangle
        /// </summary>
        /// <param name="rec">The rectangle to test against</param>
        /// <param name="line">The line to test</param>
        /// <param name="intersection1">First point where the two intersect. Vector2.Zero if no intersection</param>
        /// <param name="intersection2">Second point where the two intersect. null if no second intersection</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVRectangle(RectangleF rec, Line line, out Vector2 intersection1, out Vector2? intersection2)
        {
            return LineVRectangle(rec, line.p1, line.p2, out intersection1, out intersection2);
        }
        /// <summary>
        /// Tests for intersection between a line and a rectangle
        /// </summary>
        /// <param name="rec">The rectangle to test against</param>
        /// <param name="l1P1">The first point of the line to test</param>
        /// <param name="l1P2">The second point of the line to test</param>
        /// <param name="intersection1">First point where the two intersect. Vector2.Zero if no intersection</param>
        /// <param name="intersection2">Second point where the two intersect. null if no second intersection</param>
        /// <returns>wether it found an intersection</returns>
        public static bool LineVRectangle(RectangleF rec, Vector2 l1P1, Vector2 l1P2, out Vector2 intersection1, out Vector2? intersection2)
        {
            Vector2 rP1 = new Vector2(rec.X, rec.Y);
            Vector2 rP2 = new Vector2(rec.Right, rec.Y);
            Vector2 rP3 = new Vector2(rec.Right, rec.Bottom);
            Vector2 rP4 = new Vector2(rec.X, rec.Bottom);
            Vector2 inter = Vector2.Zero;
            intersection1 = Vector2.Zero;
            intersection2 = null;
            bool found1 = false;
            if (LineVLine(rP1, rP2, l1P1, l1P2, out inter))
            {
                if (!found1)
                {
                    intersection1 = inter;
                    found1 = true;
                }
                else
                {
                    intersection2 = inter;
                }
            }
            if (LineVLine(rP2, rP3, l1P1, l1P2, out inter))
            {
                if (!found1)
                {
                    intersection1 = inter;
                    found1 = true;
                }
                else
                {
                    intersection2 = inter;
                }
            }
            if (LineVLine(rP3, rP4, l1P1, l1P2, out inter))
            {
                if (!found1)
                {
                    intersection1 = inter;
                    found1 = true;
                }
                else
                {
                    intersection2 = inter;
                }
            }
            if (LineVLine(rP4, rP1, l1P1, l1P2, out inter))
            {
                if (!found1)
                {
                    intersection1 = inter;
                    found1 = true;
                }
                else
                {
                    intersection2 = inter;
                }
            }

            if (found1 && intersection2.HasValue)
            {
                if (Distance(intersection1, l1P1) > Distance(intersection2.Value, l1P1))
                {
                    Vector2 o = intersection1;
                    intersection1 = intersection2.Value;
                    intersection2 = o;
                }
            }

            if (found1)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Tests for intersection between a circle and a line. Accounts for both 1 intersection and 2 intersection cases
        /// </summary>
        /// <param name="circle">The circle to test against</param>
        /// <param name="line">The line to test</param>
        /// <param name="intersection1">The first intersection. Vector2.Zero if there is no intersection</param>
        /// <param name="intersection2">The second intersection. null if there is no second intersection</param>
        /// <returns>Whether or not there is at least 1 intersection</returns>
        public static bool CircleVLine(Circle circle, Line line, out Vector2 intersection1, out Vector2? intersection2)
        {
            Vector2 perpPoint = line.PerpPoint(circle.center);
            float a = Vector2.Dot(perpPoint - line.p1, Vector2.Normalize(line.p2 - line.p1));
            a /= line.Length;
            if (a >= 0 && a <= 1 && Calc.DistanceSquared(circle.center, perpPoint) < circle.radius * circle.radius)
            {
                intersection1 = perpPoint;
                intersection2 = null;
                return true;
            }
            else
            {
                if (Calc.DistanceSquared(circle.center, line.p1) < circle.radius * circle.radius)
                {
                    intersection1 = line.p1;
                    intersection2 = null;
                    return true;
                }
                if (Calc.DistanceSquared(circle.center, line.p2) < circle.radius * circle.radius)
                {
                    intersection1 = line.p2;
                    intersection2 = null;
                    return true;
                }

                intersection1 = Vector2.Zero;
                intersection2 = null;
                return false;
            }

            Circle relCircle = new Circle(Calc.RotateAround(circle.center, line.p1, -(float)line.HeadingDir), circle.radius) - line.p1;
            Line relLine = new Line(new Vector2(0, 0), new Vector2(line.Length, 0));

            double offsetDir = Math.Acos(relCircle.center.Y / circle.radius);
            double toLineDir = line.HeadingDir - MathHelper.PiOver2;
            if (relCircle.center.Y < 0)
                toLineDir += MathHelper.Pi * 2;
            Vector2 potential1 = circle.center + Calc.CirclePoint((float)(toLineDir - offsetDir), circle.radius);
            Vector2 potential2 = circle.center + Calc.CirclePoint((float)(toLineDir + offsetDir), circle.radius);

            if (relCircle.center.X < circle.radius || relCircle.center.X > line.Length + circle.radius
                || relCircle.center.Y > circle.radius || relCircle.center.Y < circle.radius)
            {
                intersection1 = Vector2.Zero;
                intersection2 = null;
                return false;
            }
            else if (relCircle.center.X < circle.radius)
            {
                intersection1 = potential2;
                intersection2 = null;
                return true;
            }
            else if (relCircle.center.X > line.Length - circle.radius)
            {
                intersection1 = potential1;
                intersection2 = null;
                return true;
            }
            else
            {

                intersection1 = potential1;
                intersection2 = potential2;
                return true;
            }
        }

        /// <summary>
        /// Checks for continuous intersection of a moving circle with a static line. 
        /// </summary>
        /// <param name="circle">The circle (at it's starting position) to be tested</param>
        /// <param name="line">The line to be tested against</param>
        /// <param name="velocity">The instantaneous velocity of the circle</param>
        /// <param name="firstAmount">value of when the circle starts to intersect the line. Actual distance/position is equal to velocity * firstAmount</param>
        /// <param name="secondAmount">value of when the circle ends it's intersection with the line. Actual distance/position is equal to velocity * secondAmount</param>
        /// <param name="intersection">The point on the line where the circle first intersects (at firstAmount)</param>
        /// <returns>Whether or not the circle will interesect this step</returns>
        public static bool CircleVLineContinuous(Circle circle, Line line, Vector2 velocity, out float firstAmount, out float secondAmount, out Vector2 intersection)
        {
            if (velocity == Vector2.Zero)
            {
                intersection = Vector2.Zero;
                firstAmount = 1f;
                secondAmount = 1f;
                return false;
            }
            Vector2 velIntersect;
            Calc.LineVLine(circle.center, circle.center + velocity, line.p1, line.p2, out velIntersect, true);

            float dist = Vector2.Dot(velIntersect - circle.center, Vector2.Normalize(velocity));
            double theta = Calc.AngleBetween(velocity, line.p2 - line.p1);
            float velLength = velocity.Length;
            float a1 = (float)(dist - (circle.radius / Math.Sin(theta) * Math.Sign(dist))) / velLength;
            float a2 = (float)(dist + (circle.radius / Math.Sin(theta) * Math.Sign(dist))) / velLength;
            firstAmount = Math.Min(a1, a2);
            secondAmount = Math.Max(a1, a2);
            Vector2 normal = Vector2.Zero;
            if (line.IsOnLeft(firstAmount * velocity + circle.center))
            {
                normal = line.RightNormal;
            }
            else
            {
                normal = line.LeftNormal;
            }
            intersection = firstAmount * velocity + circle.center + normal * circle.radius;

            if ((line.p1.X != line.p2.X && line.XRange.IsInside(intersection.X, true)) ||
                (line.p1.Y != line.p2.Y && line.YRange.IsInside(intersection.Y, true)))
            {
                return (firstAmount > -(1f / velLength) && firstAmount <= 1);
            }
            else if (CircleVPointContinuous(circle, line.p1, velocity, out firstAmount, out secondAmount))
            {
                intersection = line.p1;
                return true;
            }
            else if (CircleVPointContinuous(circle, line.p2, velocity, out firstAmount, out secondAmount))
            {
                intersection = line.p2;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks for continuous intersection of a moving circle with a static point. 
        /// </summary>
        /// <param name="circle">The circle (at it's starting position) to be tested</param>
        /// <param name="point">The point to be tested against</param>
        /// <param name="velocity">The instantaneous velocity of the circle</param>
        /// <param name="firstAmount">value of when the circle starts to intersect the point. Actual distance/position is equal to velocity * firstAmount</param>
        /// <param name="secondAmount">value of when the circle ends it's intersection with the point. Actual distance/position is equal to velocity * secondAmount</param>
        /// <returns>Whether or not the circle will interesect the point this step</returns>
        public static bool CircleVPointContinuous(Circle circle, Vector2 point, Vector2 velocity, out float firstAmount, out float secondAmount)
        {
            if (velocity == Vector2.Zero || circle.radius == 0f)
            {
                firstAmount = 1f;
                secondAmount = 1f;
                return false;
            }

            float velLength = velocity.Length;
            Line velLine = new Line(circle.center, circle.center + velocity);
            Vector2 perpPoint = velLine.PerpPoint(point);
            float dist = Calc.Distance(perpPoint, point);
            if (dist > circle.radius)
            {
                firstAmount = 1f;
                secondAmount = 1f;
                return false;
            }

            double theta = MathHelper.PiOver2 - Math.Asin(dist / circle.radius);
            double backDist = Math.Sin(theta) * circle.radius;

            //I'm reusing perpPoint right now
            perpPoint -= circle.center;
            float c = Vector2.Dot(perpPoint, Vector2.Normalize(velocity));
            float a1 = (c - (float)backDist) / velLength;
            float a2 = (c + (float)backDist) / velLength;
            firstAmount = Math.Min(a1, a2);
            secondAmount = Math.Max(a1, a2);
            if (firstAmount > -(1f / velLength) && firstAmount <= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion
    }
}
