﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using OpenTK.Input;
using QuickFont;
using System.Diagnostics;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// This class handles all of the public static items that need to be accessible by all classes.
    /// Mainly handles input from keyboard and mouse and some useful variables (rand, dot, ect.)
    /// </summary>
    class My
    {
        /// <summary>
        /// A reference to the GameWindow created in Program.cs
        /// </summary>
        public static GameWindow window;
        /// <summary>
        /// List of keys that are currently being held down
        /// </summary>
        private static List<Key> keysPressed;
        /// <summary>
        /// List of keys that was being held down last frame
        /// </summary>
        private static List<Key> keysPressedLast;
        /// <summary>
        /// List of mouse buttons currently being held down
        /// </summary>
        private static List<MouseButton> mousePressed;
        /// <summary>
        /// List of mouse buttons that were being held last frame
        /// </summary>
        private static List<MouseButton> mousePressedLast;
        public static int WheelValue, WheelValueLast;
        public static float WheelPrecise, WheelPreciseLast;

        public static int NumKeysPress
        {
            get
            {
                return keysPressed.Count;
            }
        }
        public static int NumKeysPressLast
        {
            get
            {
                return keysPressedLast.Count;
            }
        }
        public static Key[] KeysPressed
        {
            get
            {
                Key[] o = new Key[NumKeysPress];
                keysPressed.CopyTo(o);
                return o;
            }
        }
        public static Key[] KeysPressedLast
        {
            get
            {
                Key[] o = new Key[NumKeysPressLast];
                keysPressed.CopyTo(o);
                return o;
            }
        }

        /// <summary>
        /// A white 1x1 texture for drawing solid color geometry even when textures are enabled
        /// Used a lot by Spritebatch class
        /// </summary>
        public static Texture2D dot;
        /// <summary>
        /// A default font that can be used anywhere
        /// Loaded from "Content/Fonts/times.ttf" in 12pt size.
        /// Make sure your push your own options before using since this font is globally used
        /// </summary>
        public static QFont font;
        /// <summary>
        /// A Random number generator intended to be used by anyone
        /// </summary>
        public static Random rand;

        /// <summary>
        /// This keeps track of the total time since the game started.
        /// Used by everything to update correctly.
        /// </summary>
        public static Stopwatch GameTime;

        /// <summary>
        /// Initializes public static members and systems that My class provides.
        /// Creates dot texture, loads default font, and initializes a Random variable.
        /// Called from Game1
        /// </summary>
        /// <param name="gameWindow"></param>
        public static void Initialize(GameWindow gameWindow)
        {
            GameTime = new Stopwatch();
            GameTime.Start();

            window = gameWindow;
            keysPressedLast = new List<Key>();
            keysPressed = new List<Key>();
            mousePressedLast = new List<MouseButton>();
            mousePressed = new List<MouseButton>();
            window.Keyboard.KeyDown += Keyboard_KeyDown;
            window.Keyboard.KeyUp += Keyboard_KeyUp;
            window.Mouse.ButtonDown += Mouse_ButtonDown;
            window.Mouse.ButtonUp += Mouse_ButtonUp;
            window.Mouse.WheelChanged += Mouse_WheelChanged;

            rand = new Random();
            #region Create dot texture
            {
                int id = GL.GenTexture();
                GL.BindTexture(TextureTarget.Texture2D, id);

                Bitmap bmp = new Bitmap(1, 1);
                bmp.SetPixel(0, 0, Color.White);
                BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, 1, 1), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                    1, 1, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                    PixelType.UnsignedByte, bmpData.Scan0);

                dot = new Texture2D("dot", id, 1, 1);

                bmp.UnlockBits(bmpData);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMinFilter.Linear);
            }
            #endregion
            font = new QFont("Content/Fonts/times.ttf", 12);
        }

        static void Mouse_WheelChanged(object sender, MouseWheelEventArgs e)
        {
            WheelValue = e.Value;
            WheelPrecise = e.ValuePrecise;
        }

        static void Mouse_ButtonDown(object sender, MouseButtonEventArgs e)
        {
            mousePressed.Add(e.Button);
        }
        static void Mouse_ButtonUp(object sender, MouseButtonEventArgs e)
        {
            while (mousePressed.Contains(e.Button))
            {
                mousePressed.Remove(e.Button);
            }
        }

        private static void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            keysPressed.Add(e.Key);
        }
        static void Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            while (keysPressed.Contains(e.Key))
            {
                keysPressed.Remove(e.Key);
            }
        }

        /// <summary>
        /// This should be done at the beginning of the Update function
        /// before other things update. Otherwise the Key and Mouse functions
        /// might not work properly
        /// Called from Game1
        /// </summary>
        public static void Update()
        {
            //Not currently used. Consider getting rid of?
        }

        /// <summary>
        /// This should be done at the end of each Update function
        /// after everything has been updated. Otherwise the Key and Mouse
        /// functions might not work properly
        /// Called from Game1
        /// </summary>
        public static void UpdateAfter()
        {
            keysPressedLast = new List<Key>();
            //NOt sure if there is a function that will automatically
            //copy one list to the other but thought it was more safe
            //to make sure I don't make a reference on accident
            for (int i = 0; i < keysPressed.Count; i++)
            {
                keysPressedLast.Add(keysPressed[i]);
            }
            mousePressedLast = new List<MouseButton>();
            for (int i = 0; i < mousePressed.Count; i++)
            {
                mousePressedLast.Add(mousePressed[i]);
            }

            WheelValueLast = WheelValue;
            WheelPreciseLast = WheelPrecise;
        }

        /// <summary>
        /// Drops all the keys from the currently being pressed list
        /// Can be useful if keys are getting stuck on after frame skips or freezes
        /// </summary>
        public static void ResetKeysPressedList()
        {
            keysPressed = new List<Key>();
        }

        public static bool KeyPress(Key key)
        {
            return (keysPressed.Contains(key) && !keysPressedLast.Contains(key));
        }
        public static bool KeyRelease(Key key)
        {
            return (!keysPressed.Contains(key) && keysPressedLast.Contains(key));
        }
        public static bool KeyDown(Key key)
        {
            return (keysPressed.Contains(key));
        }

        public static bool MousePress(bool left)
        {
            return (left ? MousePress(MouseButton.Left) : MousePress(MouseButton.Right));
        }
        public static bool MousePress(MouseButton button)
        {
            return (mousePressed.Contains(button) && !mousePressedLast.Contains(button));
        }
        public static bool MouseRelease(MouseButton button)
        {
            return (!mousePressed.Contains(button) && mousePressedLast.Contains(button));
        }
        public static bool MouseRelease(bool left)
        {
            return (left ? MouseRelease(MouseButton.Left) : MouseRelease(MouseButton.Right));
        }
        public static bool MouseDown(MouseButton button)
        {
            return (mousePressed.Contains(button));
        }
        public static bool MouseDown(bool left)
        {
            return (left ? MouseDown(MouseButton.Left) : MouseDown(MouseButton.Right));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The amount the wheel has changed since last frame (Can be negative)</returns>
        public static int WheelChange()
        {
            return WheelValue - WheelValueLast;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>The precise amount the wheel has changed since last frame (Can be negative)</returns>
        public static float WheelChangePrecise()
        {
            return WheelPrecise - WheelPreciseLast;
        }
        public static bool WheelMovedUp()
        {
            return WheelValue > WheelValueLast;
        }
        public static bool WheelMovedDown()
        {
            return WheelValue < WheelValueLast;
        }
    }
}
