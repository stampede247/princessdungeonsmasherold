﻿using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// Holds info about a Sound File that has been loaded in and passed to OpenGL.
    /// </summary>
	struct SoundEffect
	{
		private int channels, bitsPerSample, rate, bufferId, sourceId, numSamples;
        private double length;

        /// <summary>
        /// The number of channels the sound contains
        /// </summary>
		public int Channels { get { return channels; } }
        /// <summary>
        /// The bits per sample of the sound
        /// </summary>
		public int BitsPerSample { get { return bitsPerSample; } }
        /// <summary>
        /// The sample rate of the sound
        /// </summary>
		public int Rate { get { return rate; } }
        /// <summary>
        /// The OpenAL buffer ID
        /// </summary>
		public int BufferId { get { return bufferId; } }
        /// <summary>
        /// The OpenAL source ID
        /// </summary>
		public int SourceId { get { return sourceId; } }
        /// <summary>
        /// Gets the current state of the OpenAL source ID
        /// </summary>
		public ALSourceState State
		{
			get
			{
				int ret;
				AL.GetSource(sourceId, ALGetSourcei.SourceState, out ret);
				return (ALSourceState)ret;
			}
		}
        /// <summary>
        /// The number of samples in the sound
        /// </summary>
        public int NumSamples { get { return numSamples; } }
        /// <summary>
        /// The length of the sound in seconds
        /// </summary>
        public double Length { get { return length; } }

        /// <summary>
        /// The default constructor for the SoundEffect struct
        /// </summary>//The number of bytes for one sample including all channels.
        /// <param name="bufferId">The OpenAL buffer ID</param>
        /// <param name="sourceId">The OpenAL source ID</param>
        /// <param name="channels">The number of channels in the sound</param>
        /// <param name="bitsPerSample">The number of bits per sample of the sound</param>
        /// <param name="rate">The sample rate of the sound</param>
        /// <param name="numSamples">The number of samples in the sound</param>
        /// <param name="length">The length of the sound in seconds</param>
		public SoundEffect(int bufferId, int sourceId, int channels, int bitsPerSample, int rate, int numSamples, double length)
		{
			this.bufferId = bufferId;
			this.sourceId = sourceId;
			this.channels = channels;
			this.bitsPerSample = bitsPerSample;
			this.rate = rate;
            this.numSamples = numSamples;
            this.length = length;
		}
	}
}
