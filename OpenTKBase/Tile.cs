﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{
    public enum TileType
    {
        Ground,
        Wall,
        Hole,
        Ice,
        Lava,
        Water,
        BoxDepressed,
        PushRight,
        PushDown,
        PushLeft,
        PushUp,
    }
    
    [Serializable]
    struct Tile
    {
        private TileType type;
        private bool isSolid;
        /// <summary>
        /// Represents the grid position of the sprite in the spritesheet.
        /// starting from 0,0 going to 9,9
        /// </summary>
        private int spriteSheetX, spriteSheetY;

        public TileType Type { get { return type; } }
        public bool IsSolid { get { return isSolid; } }
        public int SpriteSheetX { get { return spriteSheetX; } }
        public int SpriteSheetY { get { return spriteSheetY; } }

        public Tile(TileType type)
        {
            this.type = type;

            switch (type)
            {
                case TileType.Ground:
                    spriteSheetX = 0;
                    spriteSheetY = 0;
                    isSolid = false;
                    break;
                case TileType.Wall:
                    spriteSheetX = 1;
                    spriteSheetY = 0;
                    isSolid = true;
                    break;
                case TileType.Hole:
                    spriteSheetX = 2;
                    spriteSheetY = 0;
                    isSolid = false;
                    break;
                case TileType.Ice:
                    spriteSheetX = 3;
                    spriteSheetY = 0;
                    isSolid = false;
                    break;
                case TileType.Lava:
                    spriteSheetX = 4;
                    spriteSheetY = 0;
                    isSolid = false;
                    break;
                case TileType.Water:
                    spriteSheetX = 5;
                    spriteSheetY = 0;
                    isSolid = false;
                    break;
                case TileType.BoxDepressed:
                    spriteSheetX = 6;
                    spriteSheetY = 0;
                    isSolid = false;
                    break;
                case TileType.PushRight:
                    spriteSheetX = 0;
                    spriteSheetY = 1;
                    isSolid = false;
                    break;
                case TileType.PushDown:
                    spriteSheetX = 1;
                    spriteSheetY = 1;
                    isSolid = false;
                    break;
                case TileType.PushLeft:
                    spriteSheetX = 2;
                    spriteSheetY = 1;
                    isSolid = false;
                    break;
                case TileType.PushUp:
                    spriteSheetX = 3;
                    spriteSheetY = 1;
                    isSolid = false;
                    break;
                default:
                    spriteSheetX = 8;
                    spriteSheetY = 0;
                    isSolid = true;
                    break;
            };
        }
    }
}
