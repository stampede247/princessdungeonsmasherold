﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using OpenTK;
using System.Drawing;
using System.Collections;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// Used for parsing my custom console code
    /// </summary>
    enum CodeElementType
    {
        Object, //something followed by a dot
        Property, //something we are reading or setting
        Value, //the value we want to put in the property or in a function
        Function, //the function we want to be called
        Unknown
    }

    /// <summary>
    /// This class is an attempt to make a console capable of accessing classes, variables, and values from within the Console window.
    /// It is initialized by Game1 and activated in it's Update function when Tilde key is pressed.
    /// It allows you to browse an instance of any class and access all it's members (public and private).
    /// </summary>
    class ConsoleHandler
    {
        private object baseObject;
        public bool debugEnabled = false;

        /// <summary>
        /// The history of commands entered into the console
        /// </summary>
        public List<string> History;

        /// <summary>
        /// Initializes the ConsoleHandler. 
        /// </summary>
        /// <param name="game"></param>
        public ConsoleHandler(object baseObject)
        {
            this.baseObject = baseObject;
            History = new List<string>();
        }

        /// <summary>
        /// Starts a loop (holding the current thread) and allows you to browse variables and classes. 
        /// Is usually activated by Game1 when the Tilde key is pressed.
        /// </summary>
        public void Open()
        {
            Console.WriteLine("Console Activated. (type \"help\" for help)");
            while (true)
            {
                string inputLine = ReadInput();
                string lower = inputLine.ToLower();
                if (lower == "exit" || lower == "exit()" || lower.Contains('`'))
                {
                    WriteMessage("Returning to game...");
                    break;
                }
                else if (lower == "debug" || lower == "debug()")
                {
                    debugEnabled = !debugEnabled;

                    WriteMessage(String.Format("Debug turned {0}.", (debugEnabled ? "on" : "off")));
                }
                else if (lower == "help" || lower == "help()")
                {
                    WriteHelpMessage();
                }
                else
                {
                    if (!RunLine(inputLine))
                    {
                        WriteWarning("Unable to execute: '" + inputLine + "'");
                    }
                    History.Add(inputLine);
                }
            }

            My.ResetKeysPressedList();
        }

        /// <summary>
        /// This is called when you type "help" into the console
        /// </summary>
        private void WriteHelpMessage()
        {
            WriteMessage("");
            WriteMessage("=======Available Commands=======");
            WriteMessage("Type 'Exit' to return to game");
            WriteMessage("Type 'Debug' to enable debug information about console");
            WriteMessage("Call GetMembers() on any object to get a list of it's members");
            WriteMessage("==============Info==============");
            WriteMessage("The base object is of type '" + baseObject.GetType().ToString() + "'");
            WriteMessage("Capatalization is not important on local functions. It is important when calling C# members and functions");
            WriteMessage("============Examples============");
            WriteMessage("Reading a value:         players[0].position");
            WriteMessage("Setting a value:         players[0].position = 10.5'50");
            WriteMessage("Calling a function:      window.Close()");
            WriteMessage("To get started try this: getmembers()");
            WriteMessage("");
        }

        /// <summary>
        /// Changes console colors and writes ">> " indicating input is required. Asks user for input
        /// </summary>
        /// <returns>ReadLine() output</returns>
        public string ReadInput()
        {
            Console.BackgroundColor = ConsoleColor.DarkGray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(">> ");
            string inputLine = Console.ReadLine();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;

            return inputLine;
        }

        /// <summary>
        /// Writes something to the console using the Error format (colors)
        /// </summary>
        /// <param name="message">The text to write</param>
        private void WriteError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        /// <summary>
        /// Writes something to the console using the Warning format (colors)
        /// </summary>
        /// <param name="message">The text to write</param>
        private void WriteWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        /// <summary>
        /// Writes something to the console using the Success format (colors)
        /// </summary>
        /// <param name="message">The text to write</param>
        private void WriteSuccess(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        /// <summary>
        /// Writes something to the console using the default format (colors)
        /// </summary>
        /// <param name="message">The text to write</param>
        private void WriteMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        /// <summary>
        /// Parses code, runs commands contained, and returns values requested
        /// </summary>
        /// <param name="input">The code that needs to be parsed</param>
        /// <returns></returns>
        public bool RunLine(string input)
        {
            string line = input.Replace(" ", string.Empty);
            line = line.Replace(");", ")");

            if (!input.Contains(';') && !input.Contains(')') && !input.Contains('?'))
            {
                //uncommenting this will force them to specify with harder syntax

                if (!input.Contains('=') && !input.Contains('('))
                {
                    //Ease of use we will automatically assume you are querying a value if not explicitly written
                    line = line + "?";
                }
                else if (input.Contains('=') && !input.Contains('('))
                {
                    line = line + ";";
                }
                else
                {
                    WriteError("Syntax error. No semicolon, ending parentheses, or question mark present");
                    return false;
                }
            }

            List<string> elements = new List<string>();
            List<char> seperators = new List<char>();
            List<CodeElementType> types = new List<CodeElementType>();
            #region Parse into elements
            {
                int lastIndex = 0;
                bool insideQuotations = false;
                for (int i = 0; i < line.Length; i++)
                {
                    if (!insideQuotations)
                    {
                        if (line[i] == '.' || line[i] == '(' || line[i] == ')' || line[i] == '?'
                            || line[i] == '=' || line[i] == ';' || line[i] == ',')
                        {
                            string part = line.Substring(lastIndex, i - lastIndex);

                            elements.Add(part);
                            seperators.Add(line[i]);
                            lastIndex = i + 1;

                            if (part == "" && debugEnabled)
                            {
                                WriteError(String.Format("We had an empty element at index {0}", i));
                            }
                        }
                    }
                    if (line[i] == '"')
                    {
                        insideQuotations = !insideQuotations;
                    }
                }


                for (int i = 0; i < elements.Count; i++)
                {
                    CodeElementType newType = CodeElementType.Unknown;
                    if (seperators[i] == '.')
                    {
                        newType = CodeElementType.Object;
                    }
                    else if (seperators[i] == '=' || seperators[i] == '?')
                    {
                        newType = CodeElementType.Property;
                    }
                    else if (seperators[i] == '(')
                    {
                        newType = CodeElementType.Function;
                    }
                    else if (seperators[i] == ';' || seperators[i] == ',' || seperators[i] == ')')
                    {
                        newType = CodeElementType.Value;
                    }

                    if (newType == CodeElementType.Unknown)
                    {
                        WriteError(String.Format("Unkown element type at {0} with text '{1}' and following seperator '{0}'", i, elements[i], seperators[i]));
                        return false;
                    }

                    types.Add(newType);
                }
            }
            #endregion

            if (elements.Count == 0 || elements.Count != types.Count 
                || types.Count != seperators.Count || seperators.Count != elements.Count)
            {
                return false;
            }

            if (debugEnabled)
            {
                WriteMessage("Here's what we parsed successfully:");
                for (int i = 0; i < elements.Count; i++)
                {
                    Console.WriteLine("{0}: {1}", types[i], elements[i]);
                }
            }


            #region Execute
            {
                CodeElementType lastType = types[elements.Count - 1];
                if (lastType == CodeElementType.Property)
                {
                    #region Find value of property
                    if (debugEnabled)
                        WriteMessage("You want to get a value");

                    object property = baseObject;
                    for (int i = 0; i < elements.Count; i++)
                    {
                        if (types[i] == CodeElementType.Object)
                        {
                            string elementName = elements[i];
                            bool isTryingIndexed = false;
                            int indexValue = -1;
                            if (elementName.Contains('[') && elementName.Contains(']'))
                            {
                                int first = elementName.IndexOf('[') + 1;
                                int second = elementName.IndexOf(']');
                                if (int.TryParse(elementName.Substring(first, second - first), out indexValue))
                                {
                                    isTryingIndexed = true;
                                    elementName = elementName.Remove(elementName.IndexOf('['));
                                    if (debugEnabled)
                                    {
                                        WriteMessage(String.Format("You want to acces object {0} of {1}", indexValue, elementName));
                                    }
                                }
                            }

                            object newProperty = property.GetType().GetField(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                            if (newProperty == null)
                            {
                                WriteError(String.Format("Object of type {0} did not have object '{1}' or was not accessible", property.GetType(), elements[i]));
                                return false;
                            }
                            else
                            {
                                newProperty = (newProperty as FieldInfo).GetValue(property);
                                if (isTryingIndexed)
                                {
                                    #region Try to get object at index
                                    try
                                    {
                                        if (!(newProperty is IList))
                                        {
                                            WriteError(String.Format("Object '{0}' was not an IList and cannot be indexed", elementName));
                                            return false;
                                        }
                                        IList list = (IList)newProperty;
                                        if (indexValue < 0 || indexValue >= list.Count)
                                        {
                                            WriteError(String.Format("Invalid index {0}. Should be between {1} and {2}", indexValue, 0, list.Count - 1));
                                            return false;
                                        }

                                        newProperty = list[indexValue];
                                    }
                                    catch (Exception e)
                                    {
                                        WriteError(String.Format("Error occurred when trying to access object {0} in list {1}", indexValue, elementName));
                                        WriteError(String.Format("Error: {0}", e));
                                        return false;
                                    }
                                    #endregion
                                }
                                property = newProperty;
                            }
                        }
                        else if (types[i] == CodeElementType.Property)
                        {
                            string elementName = elements[i];
                            bool isTryingIndexed = false;
                            int indexValue = -1;
                            if (elementName.Contains('[') && elementName.Contains(']'))
                            {
                                int first = elementName.IndexOf('[') + 1;
                                int second = elementName.IndexOf(']');
                                if (int.TryParse(elementName.Substring(first, second - first), out indexValue))
                                {
                                    isTryingIndexed = true;
                                    elementName = elementName.Remove(elementName.IndexOf('['));
                                    if (debugEnabled)
                                    {
                                        WriteMessage(String.Format("You want to acces object {0} of {1}", indexValue, elementName));
                                    }
                                }
                            }


                            object newProperty = property.GetType().GetField(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                            if (newProperty == null)
                            {
                                newProperty = property.GetType().GetProperty(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                                if (newProperty == null)
                                {
                                    WriteError(String.Format("Object of type {0} did not have property '{1}' or was not accessible", property.GetType(), elements[i]));
                                    return false;
                                }
                                else if (!(newProperty as PropertyInfo).CanRead)
                                {
                                    WriteError(String.Format("Property '{0}' was not readable (had no 'get' function)", elements[i]));
                                    return false;
                                }
                                else if (debugEnabled)
                                {
                                    WriteMessage("We found a property not a field to read");
                                }
                            }

                            bool isPublic = true;
                            if (newProperty is FieldInfo)
                            {
                                isPublic = (newProperty as FieldInfo).IsPublic;
                                newProperty = (newProperty as FieldInfo).GetValue(property);
                            }
                            else if (newProperty is PropertyInfo)
                            {
                                isPublic = (newProperty as PropertyInfo).GetGetMethod().IsPublic;
                                newProperty = (newProperty as PropertyInfo).GetValue(property);
                            }
                            if (isTryingIndexed)
                            {
                                #region Try to get object at index
                                try
                                {
                                    if (!(newProperty is IList))
                                    {
                                        WriteError(String.Format("Object '{0}' was not an IList and cannot be indexed", elementName));
                                        return false;
                                    }
                                    IList list = (IList)newProperty;
                                    if (indexValue < 0 || indexValue >= list.Count)
                                    {
                                        WriteError(String.Format("Invalid index {0}. Should be between {1} and {2}", indexValue, 0, list.Count - 1));
                                        return false;
                                    }

                                    newProperty = list[indexValue];
                                }
                                catch (Exception e)
                                {
                                    WriteError(String.Format("Error occurred when trying to access object {0} in list {1}", indexValue, elementName));
                                    WriteError(String.Format("Error: {0}", e));
                                    return false;
                                }
                                #endregion
                            }
                            Type type = newProperty.GetType();

                            if (newProperty is IList)
                            {
                                WriteSuccess(String.Format("{0} {1} {2} = {3}",
                                    (isPublic ? "public" : "private"), ParseType(type), elements[i], newProperty));

                                #region Write out all elements in the list
                                for (int i2 = 0; i2 < (newProperty as IList).Count; i2++)
                                {
                                    object indexedElement = (newProperty as IList)[i2];
                                    WriteSuccess(String.Format("[{0}] = {1}", i2, indexedElement));
                                }
                                #endregion
                            }
                            else
                            {
                                WriteSuccess(String.Format("{0} {1} {2} = {3}",
                                    (isPublic ? "public" : "private"), ParseType(type), elements[i], newProperty));
                            }
                            return true;
                        }
                        else
                        {
                            WriteError(String.Format("Found type {0} when trying to return property value.", types[i]));
                            return false;
                        }
                    }
                    #endregion
                }
                else if (types.Contains(CodeElementType.Function))
                {

                    int functionIndex = types.IndexOf(CodeElementType.Function);
                    string functionName = elements[functionIndex];

                    if (functionName.ToLower() == "members" || functionName.ToLower() == "getmembers")
                    {
                        #region List members
                        if (debugEnabled)
                            WriteMessage("You want to list the members");

                        object property = baseObject;
                        string propName = "game";
                        for (int i = 0; i < elements.Count; i++)
                        {
                            if (types[i] == CodeElementType.Object)
                            {
                                string elementName = elements[i];
                                bool isTryingIndexed = false;
                                int indexValue = -1;
                                if (elementName.Contains('[') && elementName.Contains(']'))
                                {
                                    int first = elementName.IndexOf('[') + 1;
                                    int second = elementName.IndexOf(']');
                                    if (int.TryParse(elementName.Substring(first, second - first), out indexValue))
                                    {
                                        isTryingIndexed = true;
                                        elementName = elementName.Remove(elementName.IndexOf('['));
                                        if (debugEnabled)
                                        {
                                            WriteMessage(String.Format("You want to acces object {0} of {1}", indexValue, elementName));
                                        }
                                    }
                                }

                                object newProperty = property.GetType().GetField(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                                propName = elements[i];
                                if (newProperty == null)
                                {
                                    WriteError(String.Format("Object of type {0} did not have object '{1}' or was not accessible", property.GetType(), elements[i]));
                                    return false;
                                }
                                else
                                {
                                    newProperty = (newProperty as FieldInfo).GetValue(property);
                                    if (isTryingIndexed)
                                    {
                                        #region Try to get object at index
                                        try
                                        {
                                            if (!(newProperty is IList))
                                            {
                                                WriteError(String.Format("Object '{0}' was not an IList and cannot be indexed", elementName));
                                                return false;
                                            }
                                            IList list = (IList)newProperty;
                                            if (indexValue < 0 || indexValue >= list.Count)
                                            {
                                                WriteError(String.Format("Invalid index {0}. Should be between {1} and {2}", indexValue, 0, list.Count - 1));
                                                return false;
                                            }

                                            newProperty = list[indexValue];
                                        }
                                        catch (Exception e)
                                        {
                                            WriteError(String.Format("Error occurred when trying to access object {0} in list {1}", indexValue, elementName));
                                            WriteError(String.Format("Error: {0}", e));
                                            return false;
                                        }
                                        #endregion
                                    }
                                    property = newProperty;
                                }
                            }
                            else if (types[i] == CodeElementType.Function)
                            {
                                MemberInfo[] members = property.GetType().GetMembers(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                                if (members == null)
                                {
                                    WriteError(String.Format("Unable to get members from object of type {0}", property.GetType()));
                                    return false;
                                }

                                WriteSuccess(String.Format("Properties of {0} {1}", property.GetType(), propName));
                                for (int i2 = 0; i2 < members.Length; i2++)
                                {
                                    string info = "Unkown Type: " + members[i2].Name;
                                    if (members[i2].MemberType == MemberTypes.Method)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                        #region Parse the method
                                        MethodInfo method = (members[i2] as MethodInfo);
                                        ParameterInfo[] parInfo = method.GetParameters();
                                        info = String.Format("{0} {1} {2}(", 
                                            (method.IsPublic ? "public" : "private"), ParseType(method.ReturnType), method.Name);
                                        for (int i3 = 0; i3 < parInfo.Length; i3++)
                                        {
                                            info = info + String.Format("{0} {1}", ParseType(parInfo[i3].ParameterType), parInfo[i3].Name);
                                            if (i3 < parInfo.Length - 1)
                                                info = info + ",";
                                        }
                                        info = info + ")";
                                        #endregion
                                    }
                                    else if (members[i2].MemberType == MemberTypes.Property)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        #region Parse the property
                                        PropertyInfo prop = (members[i2] as PropertyInfo);
                                        info = String.Format("{0} {1} [{2} {3}]", ParseType(prop.PropertyType), prop.Name,
                                            (prop.CanRead ? " get;" : ""), (prop.CanWrite ? "set; " : ""));
                                        #endregion
                                    }
                                    else if (members[i2].MemberType == MemberTypes.Field)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Magenta;
                                        #region Parse the field
                                        FieldInfo field = (members[i2] as FieldInfo);
                                        info = String.Format("{0} {1} {2}", (field.IsPublic ? "public" : "private"), ParseType(field.FieldType), field.Name);
                                        #endregion
                                    }

                                    Console.WriteLine(info);
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                }
                                return true;
                            }
                            else
                            {
                                WriteError(String.Format("Found type {0} when trying to get members.", types[i]));
                                return false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Run Function
                        if (debugEnabled)
                            WriteMessage("You want to run a function");

                        object property = baseObject;
                        for (int i = 0; i < elements.Count; i++)
                        {
                            if (types[i] == CodeElementType.Object)
                            {
                                string elementName = elements[i];
                                bool isTryingIndexed = false;
                                int indexValue = -1;
                                if (elementName.Contains('[') && elementName.Contains(']'))
                                {
                                    int first = elementName.IndexOf('[') + 1;
                                    int second = elementName.IndexOf(']');
                                    if (int.TryParse(elementName.Substring(first, second - first), out indexValue))
                                    {
                                        isTryingIndexed = true;
                                        elementName = elementName.Remove(elementName.IndexOf('['));
                                        if (debugEnabled)
                                        {
                                            WriteMessage(String.Format("You want to acces object {0} of {1}", indexValue, elementName));
                                        }
                                    }
                                }

                                object newProperty = property.GetType().GetField(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                                if (newProperty == null)
                                {
                                    WriteError(String.Format("Object of type {0} did not have object '{1}' or was not accessible", property.GetType(), elements[i]));
                                    return false;
                                }
                                else
                                {
                                    newProperty = (newProperty as FieldInfo).GetValue(property);
                                    if (isTryingIndexed)
                                    {
                                        #region Try to get object at index
                                        try
                                        {
                                            if (!(newProperty is IList))
                                            {
                                                WriteError(String.Format("Object '{0}' was not an IList and cannot be indexed", elementName));
                                                return false;
                                            }
                                            IList list = (IList)newProperty;
                                            if (indexValue < 0 || indexValue >= list.Count)
                                            {
                                                WriteError(String.Format("Invalid index {0}. Should be between {1} and {2}", indexValue, 0, list.Count - 1));
                                                return false;
                                            }

                                            newProperty = list[indexValue];
                                        }
                                        catch (Exception e)
                                        {
                                            WriteError(String.Format("Error occurred when trying to access object {0} in list {1}", indexValue, elementName));
                                            WriteError(String.Format("Error: {0}", e));
                                            return false;
                                        }
                                        #endregion
                                    }
                                    property = newProperty;
                                }
                            }
                            else if (types[i] == CodeElementType.Function)
                            {
                                MethodInfo function = property.GetType().GetMethod(elements[i]);
                                if (function == null)
                                {
                                    WriteError(String.Format("Object of type {0} did not have function '{1}' or was not accessible", property.GetType(), elements[i]));
                                    return false;
                                }
                                ParameterInfo[] parInfo = function.GetParameters();
                                string functionFormat = "";
                                #region Make a string that represents function format
                                {
                                    string temp = "";
                                    for (int i2 = 0; i2 < parInfo.Length; i2++)
                                    {
                                        if (i2 != 0)
                                            temp += ", ";
                                        temp += ParseType(parInfo[i2].ParameterType);
                                        temp += " ";
                                        temp += parInfo[i2].Name.ToString();
                                    }
                                    functionFormat = String.Format("{0}({1})'", functionName, temp);

                                    if (debugEnabled)
                                        WriteMessage("Format: '" + functionFormat + "'");
                                }
                                #endregion

                                if (parInfo.Length > 0 && !types.Contains(CodeElementType.Value))
                                {
                                    WriteError(String.Format("No input values were given for function: '{0}'", functionFormat));
                                    return false;
                                }

                                object[] parameters = new object[parInfo.Length];

                                int indexOfValues = types.IndexOf(CodeElementType.Value);
                                //Loop through and try to parse all values inputed
                                for (int i2 = 0; i2 < parInfo.Length; i2++)
                                {
                                    if (functionIndex + 1 + i2 >= elements.Count)
                                    {
                                        WriteError(String.Format("Not enough inputs were passed to the function. Expected {0}, got {1}", parInfo.Length, i2));
                                        WriteError(String.Format("Function Format: '{0}'", functionFormat));
                                        return false;
                                    }

                                    Type type = parInfo[i2].ParameterType;
                                    object inputValue;
                                    if (!TryParseValueOfType(type, elements[functionIndex + 1 + i2], out inputValue))
                                    {
                                        WriteError(String.Format("Could not parse value '{0}' as {1} for parameter '{2}'",
                                            elements[functionIndex + 1 + i2], type, parInfo[i2].Name));
                                        return false;
                                    }

                                    //Need to cast it here since our ParseValueOfType function doesn't do it for us
                                    //if (type == typeof(System.Object))
                                    //    parameters[i2] = (System.Object)inputValue;
                                    //else
                                    parameters[i2] = inputValue;
                                }

                                try
                                {
                                    object output = function.Invoke(property, parameters);

                                    if (output != null)
                                    {
                                        string outputParsed = functionName + "(";
                                        for (int i2 = 0; i2 < parameters.Length; i2++)
                                        {
                                            if (i2 != 0)
                                                outputParsed += ", ";

                                            outputParsed = outputParsed + parameters[i2].ToString();
                                        }
                                        outputParsed = outputParsed + ") = " + output.ToString();
                                        WriteSuccess(outputParsed);
                                    }
                                    else
                                        WriteSuccess("Success");

                                    return true;
                                }
                                catch (Exception e)
                                {
                                    WriteError("Function call was unsuccessful");
                                    WriteError("Error: " + e.ToString());
                                    return false;
                                }
                            }
                            else
                            {
                                WriteError(String.Format("Found type {0} when trying to call a function.", types[i]));
                                return false;
                            }
                        }
                        #endregion
                    }
                }
                else if (lastType == CodeElementType.Value)
                {
                    #region Set Value
                    if (debugEnabled)
                        WriteMessage("You want to set a value");

                    object property = baseObject;
                    for (int i = 0; i < elements.Count; i++)
                    {
                        if (types[i] == CodeElementType.Object)
                        {
                            string elementName = elements[i];
                            bool isTryingIndexed = false;
                            int indexValue = -1;
                            if (elementName.Contains('[') && elementName.Contains(']'))
                            {
                                int first = elementName.IndexOf('[') + 1;
                                int second = elementName.IndexOf(']');
                                if (int.TryParse(elementName.Substring(first, second - first), out indexValue))
                                {
                                    isTryingIndexed = true;
                                    elementName = elementName.Remove(elementName.IndexOf('['));
                                    if (debugEnabled)
                                    {
                                        WriteMessage(String.Format("You want to acces object {0} of {1}", indexValue, elementName));
                                    }
                                }
                            }

                            object newProperty = property.GetType().GetField(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                            if (newProperty == null)
                            {
                                WriteError(String.Format("Object of type {0} did not have object '{1}' or was not accessible", property.GetType(), elements[i]));
                                return false;
                            }
                            else
                            {
                                newProperty = (newProperty as FieldInfo).GetValue(property);
                                if (isTryingIndexed)
                                {
                                    #region Try to get object at index
                                    try
                                    {
                                        if (!(newProperty is IList))
                                        {
                                            WriteError(String.Format("Object '{0}' was not an IList and cannot be indexed", elementName));
                                            return false;
                                        }
                                        IList list = (IList)newProperty;
                                        if (indexValue < 0 || indexValue >= list.Count)
                                        {
                                            WriteError(String.Format("Invalid index {0}. Should be between {1} and {2}", indexValue, 0, list.Count - 1));
                                            return false;
                                        }

                                        newProperty = list[indexValue];
                                    }
                                    catch (Exception e)
                                    {
                                        WriteError(String.Format("Error occurred when trying to access object {0} in list {1}", indexValue, elementName));
                                        WriteError(String.Format("Error: {0}", e));
                                        return false;
                                    }
                                    #endregion
                                }
                                property = newProperty;
                            }
                        }
                        else if (types[i] == CodeElementType.Property)
                        {
                            string elementName = elements[i];
                            bool isTryingIndexed = false;
                            int indexValue = -1;
                            if (elementName.Contains('[') && elementName.Contains(']'))
                            {
                                int first = elementName.IndexOf('[') + 1;
                                int second = elementName.IndexOf(']');
                                if (int.TryParse(elementName.Substring(first, second - first), out indexValue))
                                {
                                    isTryingIndexed = true;
                                    elementName = elementName.Remove(elementName.IndexOf('['));
                                    if (debugEnabled)
                                    {
                                        WriteMessage(String.Format("You want to acces object {0} of {1}", indexValue, elementName));
                                    }
                                }
                            }

                            object newProperty = property.GetType().GetField(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                            if (newProperty == null)
                            {
                                newProperty = property.GetType().GetProperty(elementName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                                if (newProperty == null)
                                {
                                    WriteError(String.Format("Object of type {0} did not have property '{1}' or was not accessible", property.GetType(), elements[i]));
                                    return false;
                                }
                                else if (!(newProperty as PropertyInfo).CanWrite)
                                {
                                    WriteError(String.Format("Property '{0}' was not writable (had no 'set' function)", elements[i]));
                                    return false;
                                }
                                else if (debugEnabled)
                                {
                                    WriteMessage("We found a property not a field to read");
                                }
                            }
                            if (newProperty is FieldInfo)
                            {
                                newProperty = (newProperty as FieldInfo).GetValue(property);
                            }
                            else if (newProperty is PropertyInfo)
                            {
                                newProperty = (newProperty as PropertyInfo).GetValue(property);
                            }

                            IList list = null;
                            if (isTryingIndexed)
                            {
                                #region Try to get object at index
                                try
                                {
                                    if (!(newProperty is IList))
                                    {
                                        WriteError(String.Format("Object '{0}' was not an IList and cannot be indexed", elementName));
                                        return false;
                                    }
                                    list = (IList)newProperty;
                                    if (indexValue < 0 || indexValue >= list.Count)
                                    {
                                        WriteError(String.Format("Invalid index {0}. Should be between {1} and {2}", indexValue, 0, list.Count - 1));
                                        return false;
                                    }

                                    newProperty = list[indexValue];
                                }
                                catch (Exception e)
                                {
                                    WriteError(String.Format("Error occurred when trying to access object {0} in list {1}", indexValue, elementName));
                                    WriteError(String.Format("Error: {0}", e));
                                    return false;
                                }
                                #endregion
                            }

                            Type type = newProperty.GetType();
                            //if (newProperty is FieldInfo)
                            //{
                            //    type = (newProperty as FieldInfo).FieldType;
                            //}
                            //else if (newProperty is PropertyInfo)
                            //{
                            //    type = (newProperty as PropertyInfo).PropertyType;
                            //}

                            object valueToSet;
                            if (!TryParseValueOfType(type, elements[elements.Count - 1], out valueToSet))
                            {
                                WriteError(String.Format("Input '{0}' could not be parsed as '{1}'", elements[elements.Count - 1], type));
                                return false;
                            }

                            bool isPublic = true;
                            if (newProperty is FieldInfo)
                            {
                                (newProperty as FieldInfo).SetValue(property, valueToSet);
                                isPublic = (newProperty as FieldInfo).IsPublic;
                            }
                            else if (newProperty is PropertyInfo)
                            {
                                (newProperty as PropertyInfo).SetValue(property, valueToSet);
                                isPublic = (newProperty as PropertyInfo).GetSetMethod().IsPublic;
                            }
                            else if (isTryingIndexed)
                            {
                                //This happens we already retrieved an object from the array
                                //newProperty = valueToSet;
                                if (list == null)
                                {
                                    WriteError("List was null and cannot be written to");
                                    return false;
                                }
                                list[indexValue] = valueToSet;
                                isPublic = true;
                            }
                            else
                            {
                                WriteError("Unable to write values...");
                                return false;
                            }


                            WriteSuccess(String.Format("{0} {1} {2} = {3}", (isPublic ? "public" : "private"), type, elements[i], valueToSet));
                            return true;
                        }
                        else
                        {
                            WriteError(String.Format("Found type {0} when trying to set a value", types[i]));
                            return false;
                        }
                    }
                    #endregion
                }
                else
                {
                    WriteError("Last element was of type object. This shouldn't really happen");
                    return false;
                }
            }
            #endregion
            return true;
        }

        /// <summary>
        /// Trys to parse a string according to specific type.
        /// Supports int, float, double, bool, Vector2, Color, char, string,  and object (which tries to parse as basic value types)
        /// </summary>
        /// <param name="type">The type of value contained in string 'line'</param>
        /// <param name="line">The string containing the parsable value</param>
        /// <param name="value">The parsed value</param>
        /// <returns>Whether or not the parse attempt was successful</returns>
        private bool TryParseValueOfType(Type type, string line, out object value)
        {
            if (type == typeof(int))
            {
                int val;
                bool success = int.TryParse(line, out val);
                value = val;
                return success;
            }
            else if (type == typeof(float))
            {
                float val;
                bool success = float.TryParse(line.Replace("'", "."), out val);
                value = val;
                return success;
            }
            else if (type == typeof(double))
            {
                double val;
                bool success = double.TryParse(line.Replace("'", "."), out val);
                value = val;
                return success;
            }
            else if (type == typeof(bool))
            {
                bool val;
                bool success = bool.TryParse(line, out val);
                value = val;
                return success;
            }
            else if (type == typeof(Vector2))
            {
                Vector2 val;
                bool success = TryParseVector2(line, out val);
                value = val;
                return success;
            }
            else if (type == typeof(Color))
            {
                Color val;
                bool success = TryParseColor(line, out val);
                value = val;
                return success;
            }
            else if (type == typeof(char))
            {
                char val;
                bool success = char.TryParse(line, out val);
                value = val;
                return success;
            }
            else if (type == typeof(string))
            {
                value = line.Replace('"'.ToString(), string.Empty);
                return true;
            }
            else if (type == typeof(object))
            {
                #region Find a type that can be parsed
                if (debugEnabled)
                {
                    WriteWarning(String.Format("Attempting to cast '{0}' as generic 'object' type", line));
                }

                if (TryParseValueOfType(typeof(int), line, out value))
                    return true;
                else if (TryParseValueOfType(typeof(float), line, out value))
                    return true;
                else if (TryParseValueOfType(typeof(double), line, out value))
                    return true;
                else if (TryParseValueOfType(typeof(bool), line, out value))
                    return true;
                else if (TryParseValueOfType(typeof(char), line, out value))
                    return true;
                else if (TryParseValueOfType(typeof(Vector2), line, out value))
                    return true;
                else
                {
                    //If nothing else do it as a string
                    value = line;
                    return true;
                }
                #endregion
            }

            WriteError(String.Format("We do not support parsing to type {0} yet", type));
            value = null;
            return false;
        }

        /// <summary>
        /// Format: 0.0'0.0
        /// </summary>
        /// <param name="line"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool TryParseVector2(string line, out Vector2 value)
        {
            if (!line.Contains("'"))
            {
                value = Vector2.Zero;
                return false;
            }

            int index = line.IndexOf("'");
            float x, y;
            if (!float.TryParse(line.Substring(0, index).Replace("'", "."), out x))
            {
                value = Vector2.Zero;
                return false;
            }
            if (!float.TryParse(line.Substring(index + 1, line.Length - (index + 1)).Replace("'", "."), out y))
            {
                value = Vector2.Zero;
                return false;
            }

            value = new Vector2(x, y);
            return true;
        }

        /// <summary>
        /// Format: Color.Red
        /// </summary>
        /// <param name="line">The string containing the parsable color value</param>
        /// <param name="value">The parsed color</param>
        /// <returns>Whether or not the string was able to be parsed</returns>
        private bool TryParseColor(string line, out Color value)
        {
            string l = line;
            if (l.Length >= 6)
            {
                if (l.ToLower().Substring(0, 6) == "color.")
                {
                    l = line.Substring(6, line.Length - 6);
                }
            }
            
            value = Color.FromName(l);
            if (value.R == 0 && value.G == 0 && value.B == 0 && l.ToLower() != "black")
            {
                return false;
            }

            return true;
        }

        public string ParseType(Type type)
        {
            return type.ToString().Split('.').Last();
        }
    }
}
