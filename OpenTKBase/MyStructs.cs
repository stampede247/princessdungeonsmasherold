﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This class contains many of my custom shape classes as well as a few other structs
//It's not perfectly documented or tested so there are probably confusing bits and a few bugs
//But these classes provide for a nicer feel to working in OpenTK
//
// List of structs:
// -Range
// -Line
// -Circle
// -Polygon
// -Triangle
// -Quad
// -OBB


namespace PrincessDungeonSmasher
{
    struct Range
    {
        public float min, max;

        public float Mid
        {
            get
            {
                return (min + max) / 2f;
            }
        }
        public float Length
        {
            get
            {
                return max - min;
            }
        }

        /// <summary>
        /// val1 != min always because it automatically finds the smaller
        /// and greater values and sets them accordingly
        /// </summary>
        public Range(float val1, float val2)
        {
            min = Math.Min(val1, val2);
            max = Math.Max(val1, val2);
        }

        public static bool IsInside(Range range, float value, bool inclusive)
        {
            if (inclusive)
            {
                return (value >= range.min && value <= range.max);
            }
            else
            {
                return (value > range.min && value < range.max);
            }
        }
        public bool IsInside(float value, bool inclusive)
        {
            if (inclusive)
            {
                return (value >= min && value <= max);
            }
            else
            {
                return (value > min && value < max);
            }
        }
        public static bool Intersects(Range r1, Range r2, bool inclusive)
        {
            if (inclusive)
            {
                return (r1.max >= r2.min && r1.min <= r2.max);
            }
            else
            {
                return (r1.max > r2.min && r1.min < r2.max);
            }
        }
        public bool Intersects(Range range, bool inclusive)
        {
            if (inclusive)
            {
                return (this.max >= range.min && this.min <= range.max);
            }
            else
            {
                return (this.max > range.min && this.min < range.max);
            }
        }

        /// <summary>
        /// Essentially returns the amount these ranges overlap with each other.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>a signed value of how to translate range1 to
        /// resolve the overlap</returns>
        public static float OverlapAmount(Range range1, Range range2)
        {
            if (range1.Mid >= range2.Mid)
            {
                return range2.max - range1.min;
            }
            else
            {
                return -(range1.max - range2.min);
            }
        }
        /// <summary>
        /// Essentially returns the amount this range overlaps with other
        /// </summary>
        /// <param name="other"></param>
        /// <returns>a signed value of how to translate this range to
        /// resolve the overlap</returns>
        public float OverlapAmount(Range other)
        {
            if (this.Mid >= other.Mid)
            {
                return other.max - this.min;
            }
            else
            {
                return -(this.max - other.min);
            }
        }

        /// <summary>
        /// Use this for non-intersecting ranges
        /// </summary>
        /// <param name="?"></param>
        /// <returns>Translation needed to intersect with other</returns>
        public float TranslationTo(Range other)
        {
            if (this.max <= other.min)
                return other.min - this.max;
            else
                return this.min - other.max;
        }

        public static Range operator +(Range r, float val)
        {
            return new Range(r.min + val, r.max + val);
        }
        public static Range operator -(Range r, float val)
        {
            return new Range(r.min - val, r.max - val);
        }
    }

    struct Line
    {
        public Vector2 p1, p2;

        public float Length
        {
            get
            {
                return Calc.Distance(p1, p2);
            }
        }
        public double HeadingDir
        {
            get
            {
                return Calc.DirectionTo(p1, p2);
            }
        }
        public float Slope
        {
            get
            {
                if (UndefinedSlope)
                {
                    Console.WriteLine("Tried to get slope of a line with undefined slope");
                    return float.MaxValue;
                }
                else
                {
                    return (p2.Y - p1.Y) / (p2.X - p1.X);
                }
            }
        }
        public Vector2 MidPoint
        {
            get
            {
                return (p1 + p2) / 2f;
            }
        }
        public Range XRange
        {
            get
            {
                return new Range(p1.X, p2.X);
            }
        }
        public Range YRange
        {
            get
            {
                return new Range(p1.Y, p2.Y);
            }
        }
        public bool UndefinedSlope
        {
            get { return p1.X == p2.X; }
        }
        public double LeftNormalDir
        {
            get
            {
                double val = HeadingDir - MathHelper.PiOver2;
                return (val < 0 ? val + MathHelper.Pi * 2 : val);
            }
        }
        public double RightNormalDir
        {
            get
            {
                double val = HeadingDir + MathHelper.PiOver2;
                return (val > MathHelper.Pi * 2 ? val - MathHelper.Pi * 2 : val);
            }
        }
        public Vector2 LeftNormal
        {
            get
            {
                return Calc.CirclePoint((float)(HeadingDir - MathHelper.PiOver2), 1f);
            }
        }
        public Vector2 RightNormal
        {
            get
            {
                return Calc.CirclePoint((float)(HeadingDir + MathHelper.PiOver2), 1f);
            }
        }
        public RectangleF BoundingRec
        {
            get
            {
                return new RectangleF(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y), 
                    Math.Max(p1.X, p2.X) - Math.Min(p1.X, p2.X), Math.Max(p1.Y, p2.Y) - Math.Min(p1.Y, p2.Y));
            }
        }
        public Vector2 GetPoint(int index)
        {
            if (index < 0 || index > 1)
                throw new ArgumentOutOfRangeException("index");

            return (index == 0) ? p1 : p2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rearrange">Will switch p1 and p2 if p1.X > p2.X when this is true</param>
        public Line(Vector2 p1, Vector2 p2, bool rearrange = false)
        {
            if (rearrange)
            {
                this.p1 = (p1.X <= p2.X) ? p1 : p2;
                this.p2 = (p1.X <= p2.X) ? p2 : p1;
            }
            else
            {
                this.p1 = p1;
                this.p2 = p2;
            }
        }

        public static bool Intersects(Line line1, Line line2, out Vector2 intersect)
        {
            return Calc.LineVLine(line1.p1, line1.p2, line2.p1, line2.p2, out intersect);
        }
        public bool Intersects(Line line, out Vector2 intersect)
        {
            return Calc.LineVLine(this.p1, this.p2, line.p1, line.p2, out intersect);
        }

        /// <summary>
        /// Returns a point between p1 and p2 when given a value
        /// between 0 and 1. 0 being p1 and 1 being p2 and 0.5
        /// being midpoint
        /// </summary>
        /// <param name="lerp">between 0 and 1</param>
        public static Vector2 GetLerpPoint(Line line, float lerp)
        {
            return line.p1 + (line.p2 - line.p1) * lerp;
        }
        /// <summary>
        /// Returns a point between p1 and p2 when given a value
        /// between 0 and 1. 0 being p1 and 1 being p2 and 0.5
        /// being midpoint
        /// </summary>
        /// <param name="lerp">between 0 and 1</param>
        public Vector2 GetLerpPoint(float lerp)
        {
            return p1 + (p2 - p1) * lerp;
        }
        /// <summary>
        /// Will return a value off the line if given
        /// an x that doesn't fall on the line
        /// </summary>
        public Vector2 GetPointAtX(float x, bool relative = false)
        {
            if (UndefinedSlope)
            {
                if (x < p1.X)
                    return p1;
                else
                    return p2;
            }
            float offset = relative ? (x) : (x - p1.X);
            return new Vector2(x, p1.Y + Slope * offset);
        }

        /// <summary>
        /// Returns whether the point is above 
        /// the (infinitely extending) line
        /// </summary>
        public static bool IsAbove(Line line, Vector2 point)
        {
            if (line.p2.X == line.p1.X)
            {
                if (line.p2.Y >= line.p1.Y)
                    return point.X <= line.p1.X;
                else
                    return point.X >= line.p1.X;
            }

            return (line.GetPointAtX(point.X).Y >= point.Y);
        }
        /// <summary>
        /// Returns whether the point is above 
        /// the (infinitely extending) line
        /// </summary>
        public bool IsAbove(Vector2 point)
        {
            if (p2.X == p1.X)
            {
                if (p2.Y >= p1.Y)
                    return point.X <= p1.X;
                else
                    return point.X >= p1.X;
            }

            return (GetPointAtX(point.X).Y >= point.Y);
            //float displace = point.X - p1.X;
            //return (displace * Slope + p1.Y <= point.Y);
        }
        /// <summary>
        /// Kind of line IsAbove but checks to see if the 
        /// point is on it's left going from p1 to p2
        /// </summary>
        public static bool IsOnLeft(Line line, Vector2 point)
        {
            if (line.p2.X == line.p1.X)
            {
                if (line.p2.Y >= line.p1.Y)
                    return point.X >= line.p1.X;
                else
                    return point.X <= line.p1.X;
            }

            if (Calc.WithinBounds(0, MathHelper.Pi, (float)(line.LeftNormalDir)))
                return !line.IsAbove(point);
            else
                return line.IsAbove(point);
        }
        /// <summary>
        /// Kind of line IsAbove but checks to see if the 
        /// point is on it's left going from p1 to p2
        /// </summary>
        public bool IsOnLeft(Vector2 point)
        {
            if (p2.X == p1.X)
            {
                if (p2.Y >= p1.Y)
                    return point.X >= p1.X;
                else
                    return point.X <= p1.X;
            }

            if (Calc.WithinBounds(0, MathHelper.Pi, (float)(LeftNormalDir)))
                return !IsAbove(point);
            else
                return IsAbove(point);
        }

        /// <summary>
        /// Finds a point on the line that (with testPoint) forms a perpendicular line
        /// This is the closest point on this line to testPoint
        /// </summary>
        public static Vector2 PerpPoint(Line line, Vector2 testPoint)
        {
            Vector2 p1 = line.p1;
            Vector2 p2 = line.p2;
            Vector2 p3 = testPoint;
            Vector2 p4 = testPoint + line.RightNormal;
            Vector2 intersection = Vector2.Zero;
            return new Vector2(
                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)),

                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)));
        }
        /// <summary>
        /// Finds a point on the line that (with testPoint) forms a perpendicular line
        /// This is the closest point on this line to testPoint
        /// </summary>
        public Vector2 PerpPoint(Vector2 testPoint)
        {
            Vector2 p1 = this.p1;
            Vector2 p2 = this.p2;
            Vector2 p3 = testPoint;
            Vector2 p4 = testPoint + this.RightNormal;
            Vector2 intersection = Vector2.Zero;
            return new Vector2(
                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)),

                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)));
        }

        public static Line RotateAround(Line line, Vector2 origin, float radians)
        {
            return new Line(Calc.RotateAround(line.p1, origin, radians), Calc.RotateAround(line.p2, origin, radians));
        }
        public void RotateAround(Vector2 origin, float radians)
        {
            this.p1 = Calc.RotateAround(this.p1, origin, radians);
            this.p2 = Calc.RotateAround(this.p2, origin, radians);
        }
        public static Line Translate(Line line, Vector2 offset)
        {
            return line + offset;
        }
        public void Translate(Vector2 offset)
        {
            this.p1 += offset;
            this.p2 += offset;
        }

        public static Line operator +(Line line, Vector2 offset)
        {
            return new Line(line.p1 + offset, line.p2 + offset);
        }
        public static Line operator -(Line line, Vector2 offset)
        {
            return new Line(line.p1 - offset, line.p2 - offset);
        }
    }

    struct Circle
    {
        public Vector2 center;
        public float radius;

        public float Area
        {
            get
            {
                return (float)(MathHelper.Pi * Math.Pow(radius, 2));
            }
        }
        public float Circumference
        {
            get
            {
                return (float)(2 * MathHelper.Pi * radius);
            }
        }
        /// <summary>
        /// A rectangle that encompasses the circle
        /// </summary>
        public RectangleF ColRec
        {
            get
            {
                return new RectangleF(center.X - radius, center.Y - radius, radius * 2, radius * 2);
            }
        }
        /// <summary>
        /// Returns the biggest rectangle that fits inside the circle
        /// Needs to be tested
        /// </summary>
        public RectangleF InnerRec
        {
            get
            {
                float offset = (float)(Math.Cos(MathHelper.PiOver2) * radius);
                return new RectangleF(center.X - offset, center.Y - offset, offset * 2, offset * 2);
            }
        }

        public Circle(Vector2 center, float radius)
        {
            this.center = center;
            this.radius = radius;
        }
        public Circle(RectangleF rec)
        {
            this.radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            this.center = new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f);
        }
        public Circle(Rectangle rec)
        {
            this.radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            this.center = new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f);
        }

        public static bool Intersects(Circle c1, Circle c2)
        {
            return (Calc.Distance(c1.center, c2.center) < c1.radius + c2.radius);
        }
        public bool Intersects(Circle circle)
        {
            return (Calc.Distance(this.center, circle.center) < this.radius + circle.radius);
        }

        public static bool IsInside(Circle circle, Vector2 point)
        {
            return (Calc.Distance(circle.center, point) < circle.radius);
        }
        public bool IsInside(Vector2 point)
        {
            return (Calc.Distance(this.center, point) < this.radius);
        }
        public bool IsInside(float x, float y)
        {
            return (Calc.Distance(this.center, new Vector2(x,y)) < this.radius);
        }

        public static Circle RotateAround(Circle circle, Vector2 origin, float radians)
        {
            return new Circle(Calc.RotateAround(circle.center, origin, radians), circle.radius);
        }
        public void RotateAround(Vector2 origin, float radians)
        {
            this.center = Calc.RotateAround(this.center, origin, radians);
        }
        public static Circle Translate(Circle circle, Vector2 offset)
        {
            return circle + offset;
        }
        public void Translate(Vector2 offset)
        {
            this.center += offset;
        }

        public static Circle FromRectangle(Rectangle rec)
        {
            float radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            return new Circle(new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f), radius);
        }
        public static Circle FromRectangle(RectangleF rec)
        {
            float radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            return new Circle(new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f), radius);
        }

        public static Circle operator +(Circle circle, Vector2 offset)
        {
            return new Circle(circle.center + offset, circle.radius);
        }
        public static Circle operator -(Circle circle, Vector2 offset)
        {
            return new Circle(circle.center - offset, circle.radius);
        }
    }

    struct Polygon
    {
        public Vector2[] verts;

        public int NumVerts
        {
            get
            {
                return verts.Length;
            }
        }
        /// <summary>
        /// A complex polygon is one where lines intersect
        /// </summary>
        public bool IsComplex
        {
            get
            {
                for (int i1 = 0; i1 < NumVerts - 1; i1++)
                {
                    int i2 = (i1 + 1) % NumVerts;
                    for (int i3 = i1 + 1; i3 < NumVerts; i3++)
                    {
                        int i4 = (i3 + 1) % NumVerts;
                        Vector2 intersect;
                        if (Calc.LineVLine(verts[i1], verts[i2], verts[i3], verts[i4], out intersect, false))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }
        /// <summary>
        /// Might not work?
        /// </summary>
        public bool IsClockwise
        {
            get
            {
                int j, k;
                int count = 0;
                double z;

                if (NumVerts < 3)
                    return false;

                for (int i = 0; i < NumVerts; i++)
                {
                    j = (i + 1) % NumVerts;
                    k = (i + 2) % NumVerts;
                    z = (verts[j].X - verts[i].X) * (verts[k].Y - verts[j].Y);
                    z -= (verts[j].Y - verts[i].Y) * (verts[k].X - verts[j].X);

                    if (z < 0)
                        count--;
                    else if (z > 0)
                        count++;
                }

                return (count < 0);
            }
        }
        public bool IsConvex
        {
            get
            {
                int j, k;
                int flag = 0;
                double z;

                if (NumVerts < 3)
                    return true;

                for (int i = 0; i < NumVerts; i++)
                {
                    j = (i + 1) % NumVerts;
                    k = (i + 2) % NumVerts;
                    z = (verts[j].X - verts[i].X) * (verts[k].Y - verts[j].Y);
                    z -= (verts[j].Y - verts[i].Y) * (verts[k].X - verts[j].X);

                    if (z < 0)
                        flag |= 1;
                    else if (z > 0)
                        flag |= 2;

                    if (flag == 3)
                        return false;
                }

                if (flag != 0)
                    return true;
                else
                {
                    //Colinear points but we will return true
                    return true;
                }
            }
        }
        public Vector2 Center
        {
            get
            {
                Vector2 total = Vector2.Zero;
                for (int i = 0; i < NumVerts; i++)
                {
                    total += verts[i];
                }

                return total / (float)NumVerts;
            }
        }
        public Vector2 GetVertex(int index)
        {
            if (index < 0)
                return verts[0];
            if (index >= NumVerts)
                return verts[NumVerts - 1];

            return verts[index];
        }
        public Line GetEdge(int index)
        {
            if (index < 0)
                return new Line(verts[0], verts[1]);
            if (index >= NumVerts)
                return new Line(verts[NumVerts - 2], verts[NumVerts - 1]);

            return new Line(verts[index], verts[(index + 1) % NumVerts]);
        }

        public Polygon(int numberOfVertices)
        {
            verts = new Vector2[numberOfVertices];
        }
        public Polygon(Vector2 p1, Vector2? p2 = null, Vector2? p3 = null, Vector2? p4 = null, Vector2? p5 = null)
        {
            int num = 1;
            if (p2.HasValue)
                num++;
            if (p3.HasValue)
                num++;
            if (p4.HasValue)
                num++;
            if (p5.HasValue)
                num++;

            verts = new Vector2[num];

            verts[0] = p1;
            int i = 1;
            if (p2.HasValue)
            {
                verts[i] = p2.Value;
                i++;
            }
            if (p3.HasValue)
            {
                verts[i] = p3.Value;
                i++;
            }
            if (p4.HasValue)
            {
                verts[i] = p4.Value;
                i++;
            }
            if (p5.HasValue)
            {
                verts[i] = p5.Value;
                i++;
            }
        }
        public Polygon(Vector2[] vertices)
        {
            this.verts = vertices;
        }

    }

    struct Triangle
    {
        public Vector2 v1, v2, v3;

        public bool IsClockwise
        {
            get
            {
                return !Line.IsOnLeft(new Line(v1, v2), v3);
            }
        }
        public Vector2 Center
        {
            get
            {
                return (v1 + v2 + v3) / 3f;
            }
        }
        /// <summary>
        /// Needs to be tested
        /// </summary>
        public float Area
        {
            get
            {
                float a = Calc.Distance(v1, v2);
                float b = Calc.Distance(v2, v3);
                float c = Calc.Distance(v3, v1);
                float s = (a + b + c) / 2f;
                return (float)Math.Sqrt(s * (s - a) * (s - b) * (s - c));
            }
        }
        public Line L1
        {
            get
            {
                return new Line(v1, v2);
            }
        }
        public Line L2
        {
            get
            {
                return new Line(v2, v3);
            }
        }
        public Line L3
        {
            get
            {
                return new Line(v3, v1);
            }
        }
        public Vector2 GetVertex(int index)
        {
            if (index < 0 || index > 2)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    return v2;
                case 2:
                    return v3;
                default:
                    return v1;
            };
        }
        public void SetVertex(int index, Vector2 value)
        {
            if (index < 0 || index > 2)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    v2 = value;
                    break;
                case 2:
                    v3 = value;
                    break;
                default:
                    v1 = value;
                    break;
            };
        }
        public Line GetEdge(int index)
        {
            if (index < 0 || index > 2)
                throw new ArgumentOutOfRangeException("index");

            return new Line(GetVertex(index), GetVertex((index + 1) % 3));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rearrange">
        /// Will rearrange them to clockwise if
        /// they were defined counter clockwise
        /// </param>
        public Triangle(Vector2 v1, Vector2 v2, Vector2 v3, bool rearrange = false)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            if (rearrange && !IsClockwise)
            {
                ReverseOrder();
            }
        }

        public void ReverseOrder()
        {
            Vector2 v1Old = v1;
            this.v1 = v3;
            this.v3 = v1Old;
        }

        public static Triangle RotateAround(Triangle triangle, Vector2 origin, float radians)
        {
            return new Triangle(
                Calc.RotateAround(triangle.v1, origin, radians),
                Calc.RotateAround(triangle.v2, origin, radians),
                Calc.RotateAround(triangle.v3, origin, radians));
        }
        public void RotateAround(Vector2 origin, float radians)
        {
            this.v1 = Calc.RotateAround(this.v1, origin, radians);
            this.v2 = Calc.RotateAround(this.v2, origin, radians);
            this.v3 = Calc.RotateAround(this.v3, origin, radians);
        }

        public static bool Contains(Triangle triangle, Vector2 point)
        {
            if (triangle.IsClockwise)
            {
                return (
                    !triangle.L1.IsOnLeft(point) &&
                    !triangle.L2.IsOnLeft(point) &&
                    !triangle.L3.IsOnLeft(point));
            }
            else
            {
                return (
                    triangle.L1.IsOnLeft(point) &&
                    triangle.L2.IsOnLeft(point) &&
                    triangle.L3.IsOnLeft(point));
            }
        }
        public bool Contains(Vector2 point)
        {
            if (IsClockwise)
            {
                return (
                    !L1.IsOnLeft(point) &&
                    !L2.IsOnLeft(point) &&
                    !L3.IsOnLeft(point));
            }
            else
            {
                return (
                    L1.IsOnLeft(point) &&
                    L2.IsOnLeft(point) &&
                    L3.IsOnLeft(point));
            }
        }

        public static Triangle operator +(Triangle triangle, Vector2 offset)
        {
            return new Triangle(triangle.v1 + offset, triangle.v2 + offset, triangle.v3 + offset);
        }
        public static Triangle operator -(Triangle triangle, Vector2 offset)
        {
            return new Triangle(triangle.v1 - offset, triangle.v2 - offset, triangle.v3 - offset);
        }
        public static explicit operator Polygon(Triangle t)
        {
            Polygon poly = new Polygon(3);
            poly.verts[0] = t.v1;
            poly.verts[1] = t.v2;
            poly.verts[2] = t.v3;

            return poly;
        }
    }

    struct Quad
    {
        public Vector2 v1, v2, v3, v4;

        public bool IsComplex
        {
            get
            {
                for (int i1 = 0; i1 < 4 - 1; i1++)
                {
                    int i2 = (i1 + 1) % 4;
                    for (int i3 = i1 + 1; i3 < 4; i3++)
                    {
                        int i4 = (i3 + 1) % 4;
                        Vector2 intersect;
                        if (Calc.LineVLine(GetVertex(i1), GetVertex(i2), GetVertex(i3), GetVertex(i4), out intersect, false))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }
        public bool IsClockwise
        {
            get
            {
                int i, j, k;
                int count = 0;

                for (i = 0; i < 4; i++)
                {
                    j = (i + 1) % 4;
                    k = (i == 0) ? (3) : (i - 1);
                    bool isRightTurn = new Line(Vector2.Zero, GetVertex(j) - GetVertex(i)).IsOnLeft(GetVertex(i) - GetVertex(k));

                    count += (isRightTurn ? 1 : -1);
                }

                return (count >= 0);
            }
        }
        public bool IsConvex
        {
            get
            {
                int j, k;
                int flag = 0;
                double z;

                for (int i = 0; i < 4; i++)
                {
                    j = (i + 1) % 4;
                    k = (i + 2) % 4;
                    z = (GetVertex(j).X - GetVertex(i).X) * (GetVertex(k).Y - GetVertex(j).Y);
                    z -= (GetVertex(j).Y - GetVertex(i).Y) * (GetVertex(k).X - GetVertex(j).X);

                    if (z < 0)
                        flag |= 1;
                    else if (z > 0)
                        flag |= 2;

                    if (flag == 3)
                        return false;
                }

                if (flag != 0)
                    return true;
                else
                {
                    //Colinear points but we will return true
                    return true;
                }
            }
        }
        public Vector2 Center
        {
            get
            {
                return (v1 + v2 + v3 + v4) / 4f;
            }
        }
        public Vector2 GetVertex(int index)
        {
            if (index < 0 || index > 3)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    return v2;
                case 2:
                    return v3;
                case 3:
                    return v4;
                default:
                    return v1;
            };
        }
        public void SetVertex(int index, Vector2 value)
        {
            if (index < 0 || index > 3)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    v2 = value;
                    break;
                case 2:
                    v3 = value;
                    break;
                case 3:
                    v4 = value;
                    break;
                default:
                    v1 = value;
                    break;
            };
        }
        public Line GetEdge(int index)
        {
            if (index < 0 || index > 3)
                throw new ArgumentOutOfRangeException("index");

            return new Line(GetVertex(index), GetVertex((index + 1) % 4));
        }

        public Quad(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            this.v1 = p1;
            this.v2 = p2;
            this.v3 = p3;
            this.v4 = p4;
        }
        public Quad(OBB rec)
        {
            this.v1 = rec.TopLeft;
            this.v2 = rec.TopRight;
            this.v3 = rec.BottomRight;
            this.v4 = rec.BottomLeft;
        }
        public Quad(Rectangle rec)
        {
            this.v1 = new Vector2(rec.X, rec.Y);
            this.v2 = new Vector2(rec.Right, rec.Y);
            this.v3 = new Vector2(rec.Right, rec.Bottom);
            this.v4 = new Vector2(rec.X, rec.Bottom);
        }
        public Quad(RectangleF rec)
        {
            this.v1 = new Vector2(rec.X, rec.Y);
            this.v2 = new Vector2(rec.Right, rec.Y);
            this.v3 = new Vector2(rec.Right, rec.Bottom);
            this.v4 = new Vector2(rec.X, rec.Bottom);
        }
        public Quad(Vector2[] verts)
        {
            if (verts.Length < 4 || verts.Length > 4)
                throw new ArgumentException("The vertex array was not 4 nodes long.");

            this.v1 = verts[0];
            this.v2 = verts[1];
            this.v3 = verts[2];
            this.v4 = verts[3];
        }

        public static explicit operator Polygon(Quad q)
        {
            Polygon poly = new Polygon(4);
            poly.verts[0] = q.v1;
            poly.verts[1] = q.v2;
            poly.verts[2] = q.v3;
            poly.verts[3] = q.v4;

            return poly;
        }
    }

    struct OBB
    {
        public Vector2 center;
        public Vector2 size;
        public double rotation;

        public float X
        {
            get
            {
                return center.X;
            }
            set
            {
                center.X = value;
            }
        }
        public float Y
        {
            get
            {
                return center.Y;
            }
            set
            {
                center.Y = value;
            }
        }
        public float Width
        {
            get
            {
                return size.X;
            }
            set
            {
                size.X = value;
            }
        }
        public float Height
        {
            get
            {
                return size.Y;
            }
            set
            {
                size.Y = value;
            }
        }
        public float rotationF
        {
            get
            {
                return (float)rotation;
            }
            set
            {
                rotation = (double)value;
            }
        }
        public Vector2 HalfSize
        {
            get
            {
                return size / 2f;
            }
            set
            {
                size = value * 2f;
            }
        }
        public Vector2 TopLeft
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation - MathHelper.PiOver4 * 3 + offset, HalfSize.Length);
            }
        }
        public Vector2 TopRight
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation - MathHelper.PiOver4 - offset, HalfSize.Length);
            }
        }
        public Vector2 BottomLeft
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation + MathHelper.PiOver4 * 3 - offset, HalfSize.Length);
            }
        }
        public Vector2 BottomRight
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation + MathHelper.PiOver4 + offset, HalfSize.Length);
            }
        }
        public float MinX
        {
            get
            {
                return (float)Math.Min(TopLeft.X,
                              Math.Min(TopRight.X,
                              Math.Min(BottomLeft.X, BottomRight.X)));
            }
        }
        public float MinY
        {
            get
            {
                return (float)Math.Min(TopLeft.Y,
                              Math.Min(TopRight.Y,
                              Math.Min(BottomLeft.Y, BottomRight.Y)));
            }
        }
        public float MaxX
        {
            get
            {
                return (float)Math.Max(TopLeft.X,
                              Math.Max(TopRight.X,
                              Math.Max(BottomLeft.X, BottomRight.X)));
            }
        }
        public float MaxY
        {
            get
            {
                return (float)Math.Max(TopLeft.Y,
                              Math.Max(TopRight.Y,
                              Math.Max(BottomLeft.Y, BottomRight.Y)));
            }
        }
        public RectangleF FloatRec
        {
            get
            {
                if (rotation == 0.0)
                {
                    return new RectangleF(center.X - HalfSize.X, center.Y - HalfSize.Y, size.X, size.Y);
                }
                return new RectangleF(
                    (MinX), (MinY),
                    (MaxX - MinX), (MaxY - MinY));
            }
        }
        /// <summary>
        /// Used for seperating axis thereom
        /// </summary>
        public Vector2 AxisX
        {
            get
            {
                Vector2 axis = TopRight - TopLeft;
                return (axis / axis.LengthSquared);
            }
        }
        /// <summary>
        /// Used for seperating axis thereom
        /// </summary>
        public Vector2 AxisY
        {
            get
            {
                Vector2 axis = BottomLeft - TopLeft;
                return (axis / axis.LengthSquared);
            }
        }

        public OBB(Vector2 center, Vector2 size, float rotation = 0f)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = size;
        }
        public OBB(Vector2 center, Vector2 size, double rotation = 0.0)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = size;
        }
        public OBB(Vector2 center, float width, float height, float rotation = 0f)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }
        public OBB(Vector2 center, float width, float height, double rotation = 0.0)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }
        public OBB(float centerX, float centerY, float width, float height, float rotation = 0f)
        {
            this.center = new Vector2(centerX, centerY);
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }
        public OBB(float centerX, float centerY, float width, float height, double rotation = 0.0)
        {
            this.center = new Vector2(centerX, centerY);
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }

        /// <summary>
        /// Maybe not the fastest but the easiest to program
        /// </summary>
        public static bool Contains(OBB rec, Vector2 point)
        {
            Vector2 relPos = Calc.RotateAround(point, rec.center, -rec.rotationF) - rec.center;
            return (Calc.WithinBounds(-rec.Width / 2f, rec.Width / 2f, relPos.X)
                 && Calc.WithinBounds(-rec.Height / 2f, rec.Height / 2f, relPos.Y));
        }
        /// <summary>
        /// Maybe not the fastest but the easiest to program
        /// </summary>
        public bool Contains(Vector2 point)
        {
            Vector2 relPos = Calc.RotateAround(point, center, -rotationF) - center;
            return (Calc.WithinBounds(-Width / 2f, Width / 2f, relPos.X)
                 && Calc.WithinBounds(-Height / 2f, Height / 2f, relPos.Y));
        }

        public static bool OverlapsOneWay(OBB ths, OBB other)
        {
            //Loop through the axises (on this rectangle)
            for (int a = 0; a < 2; a++)
            {
                Vector2 axis = Vector2.Zero;
                if (a == 0)
                    axis = ths.AxisX;
                else
                    axis = ths.AxisY;
                float origin = Vector2.Dot(ths.TopLeft, axis);

                double t = Vector2.Dot(other.TopLeft, axis);
                double tMin = t;
                double tMax = t;
                //Loop through the corners
                for (int c = 0; c < 4; c++)
                {
                    Vector2 corner = other.GetCorner(c);

                    t = Vector2.Dot(corner, axis);

                    if (t < tMin)
                        tMin = t;
                    else if (t > tMax)
                        tMax = t;
                }

                if (tMin > 1 + origin || tMax < origin)
                {
                    //Found a dimension where they overlapped
                    return false;
                }
            }

            //There was no dimension where they didn't overlap
            return true;
        }
        public static bool Intersects(OBB rec1, OBB rec2)
        {
            return (OverlapsOneWay(rec1, rec2) && OverlapsOneWay(rec2, rec1));
        }
        public bool Intersects(OBB other)
        {
            return (OverlapsOneWay(this, other) && OverlapsOneWay(other, this));
        }

        public Vector2 GetCorner(int index)
        {
            switch (index)
            {
                case 0:
                    return TopLeft;
                case 1:
                    return TopRight;
                case 2:
                    return BottomLeft;
                default:
                    return BottomRight;
            };
        }
        public Range GetRangeOnAxis(Vector2 axis)
        {
            Range r = new Range(0, 0);
            r.min = float.MaxValue;
            r.max = float.MinValue;
            for (int i = 0; i < 4; i++)
            {
                float value = Vector2.Dot(GetCorner(i), axis);

                if (value < r.min)
                    r.min = value;
                if (value > r.max)
                    r.max = value;
            }

            return r;
        }
        public Vector2 FindMaxPointOnAxis(Vector2 axis)
        {
            float max = float.MinValue;
            int index = -1;
            for (int i = 0; i < 4; i++)
            {
                float dot = Vector2.Dot(GetCorner(i), axis);
                if (dot > max)
                {
                    max = dot;
                    index = i;
                }
            }

            if (index == -1)
            {
                Console.WriteLine("Something went wrong in FindMaxPointOnAxis. Did not find any points");
                return this.center;
            }

            return GetCorner(index);
        }
        public Vector2 FindMinPointOnAxis(Vector2 axis)
        {
            float max = float.MinValue;
            int index = -1;
            for (int i = 0; i < 4; i++)
            {
                float dot = Vector2.Dot(GetCorner(i), axis);
                if (dot > max)
                {
                    max = dot;
                    index = i;
                }
            }

            if (index == -1)
            {
                Console.WriteLine("Something went wrong in FindMaxPointOnAxis. Did not find any points");
                return this.center;
            }

            return GetCorner(index);
        }
        /// <summary>
        /// This checks for intersection between OBBs and finds the best normal to solve it
        /// as well as tells you the position where they meet after solving.
        /// </summary>
        /// <param name="amountToMove">Double's as a normal vector (if normalized)
        /// and the translation needed to resolve the collision
        /// This is a full translation and should be broken between the two objects
        /// depending on masses of each.</param>
        /// <param name="intersection">The point (in world coordinates) where the two 
        /// OBBs are touching</param>
        /// <returns>Whether they intersect</returns>
        public static bool IntersectionPoint(OBB rec1, OBB rec2, out Vector2 amountToMove, out Vector2 intersection)
        {
            Vector2[] axises = new Vector2[4]
            {
                Vector2.Normalize(rec1.AxisX), Vector2.Normalize(rec1.AxisY),
                Vector2.Normalize(rec2.AxisX), Vector2.Normalize(rec2.AxisY)
            };

            Range[] rec1Ranges = new Range[4];
            Range[] rec2Ranges = new Range[4];
            for (int i = 0; i < 4; i++)
            {
                rec1Ranges[i] = rec1.GetRangeOnAxis(axises[i]);
                rec2Ranges[i] = rec2.GetRangeOnAxis(axises[i]);
            }

            float minOverlap = float.MaxValue;
            int minOverlapAxis = -1;
            for (int i = 0; i < 4; i++)
            {
                float overlap = rec1Ranges[i].OverlapAmount(rec2Ranges[i]);
                if (Math.Abs(overlap) < Math.Abs(minOverlap))
                {
                    minOverlap = overlap;
                    minOverlapAxis = i;
                }
            }

            if (minOverlapAxis == -1)
            {
                Console.WriteLine("Something went wrong with finding the minimum intersect axis");
                amountToMove = Vector2.Zero;
                intersection = Vector2.Zero;
                return false;
            }

            Vector2 normal = axises[minOverlapAxis];

            Vector2 minOnAxisPoint = Vector2.Zero, maxOnAxisPoint = Vector2.Zero;
            {
                float minOnAxis = float.MaxValue, maxOnAxis = float.MinValue;
                int minOnAxisIndex = -1, maxOnAxisIndex = -1;
                #region Find Min and Max Points
                if (minOverlapAxis >= 2)
                {
                    //The axis came from rec1
                    for (int i = 0; i < 4; i++)
                    {
                        float dot = Vector2.Dot(rec1.GetCorner(i), normal);
                        if (dot < minOnAxis)
                        {
                            minOnAxis = dot;
                            minOnAxisPoint = rec1.GetCorner(i);
                            minOnAxisIndex = i;
                        }
                        if (dot > maxOnAxis)
                        {
                            maxOnAxis = dot;
                            maxOnAxisPoint = rec1.GetCorner(i);
                            maxOnAxisIndex = i;
                        }
                    }
                }
                else
                {
                    //The axis came from rec2
                    for (int i = 0; i < 4; i++)
                    {
                        float dot = Vector2.Dot(rec2.GetCorner(i), normal);
                        if (dot < minOnAxis)
                        {
                            minOnAxis = dot;
                            minOnAxisPoint = rec2.GetCorner(i);
                            minOnAxisIndex = i;
                        }
                        if (dot > maxOnAxis)
                        {
                            maxOnAxis = dot;
                            maxOnAxisPoint = rec2.GetCorner(i);
                            maxOnAxisIndex = i;
                        }
                    }
                }
                #endregion
            }

            if (minOverlap >= 0)
            {
                intersection = minOnAxisPoint;
                if (minOverlapAxis < 2)
                    intersection = maxOnAxisPoint;
            }
            else
            {
                intersection = maxOnAxisPoint;
                if (minOverlapAxis < 2)
                    intersection = minOnAxisPoint;
            }
            intersection += normal * minOverlap;

            amountToMove = normal * minOverlap;

            return true;
        }

        public static bool Intersects(OBB rec, Line line, out Vector2 intersection1, out Vector2? intersection2)
        {
            Line newLine = Line.RotateAround(line, rec.center, -rec.rotationF);
            newLine.p1 -= rec.center;
            newLine.p2 -= rec.center;
            Vector2 i1;
            Vector2? i2;
            bool intersects = Calc.LineVRectangle(new RectangleF(-rec.Width / 2f, -rec.Height / 2f, rec.Width, rec.Height), line, out i1, out i2);
            if (!intersects)
            {
                intersection1 = Vector2.Zero;
                intersection2 = null;
                return false;
            }
            intersection1 = Calc.RotateAround(i1, Vector2.Zero, rec.rotationF) + rec.center;
            if (i2.HasValue)
                intersection2 = Calc.RotateAround(i2.Value, Vector2.Zero, rec.rotationF) + rec.center;
            else
                intersection2 = null;
            return true;
        }
        public bool Intersects(Line line, out Vector2 intersection1, out Vector2? intersection2)
        {
            Line newLine = Line.RotateAround(line, center, -rotationF) - center;
            //newLine.p1 = Calc.RotateAround(line.p1, center, -rotationF) - center;
            //newLine.p2 = Calc.RotateAround(line.p2, center, -rotationF) - center;
            Vector2 i1;
            Vector2? i2;
            bool intersects = Calc.LineVRectangle(new RectangleF(-Width / 2f, -Height / 2f, Width, Height), newLine, out i1, out i2);
            if (!intersects)
            {
                intersection1 = Vector2.Zero;
                intersection2 = null;
                return false;
            }
            intersection1 = Calc.RotateAround(i1, Vector2.Zero, rotationF) + center;
            if (i2.HasValue)
                intersection2 = Calc.RotateAround(i2.Value, Vector2.Zero, rotationF) + center;
            else
                intersection2 = null;
            return true;
        }

        public static OBB Grow(OBB rec, float amount)
        {
            return new OBB(rec.center, rec.size + new Vector2(amount * 2, amount * 2), rec.rotation);
        }
        public void Grow(float amount)
        {
            size = new Vector2(size.X + amount * 2, size.Y + amount * 2);
        }
        public static OBB Translate(OBB rec, Vector2 amount)
        {
            return new OBB(rec.center + amount, rec.size, rec.rotation);
        }
        public void Translate(Vector2 amount)
        {
            center += amount;
        }

        public static explicit operator Polygon(OBB o)
        {
            Polygon poly = new Polygon(4);
            poly.verts[0] = o.TopLeft;
            poly.verts[1] = o.TopRight;
            poly.verts[2] = o.BottomRight;
            poly.verts[3] = o.BottomLeft;

            return poly;
        }
    }

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}
