﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace PrincessDungeonSmasher
{
	class MusicHandler
	{
		private static int sourceID = -1;
		private static float volume = 1f;
		private static int fadeCurrent, fadeTime;
		public static bool fading, fadingIn;

		public static float Volume
		{
			get
			{
				return volume;
			}
			set
			{
				volume = value;
				if (sourceID == -1)
					sourceID = AL.GenSource();

				AL.Source(sourceID, ALSourcef.Gain, value);
			}
		}
		private static float Fade
		{
			get
			{
				if (fading) 
				{
					if (fadingIn) return ((float)fadeCurrent / fadeTime);
					else return 1f - ((float)fadeCurrent / fadeTime);
				}
				else return 1f;
			}
		}
        public static ALSourceState MusicState
        {
            get
            {
                if (sourceID == -1)
                    return ALSourceState.Stopped;
                else
                    return AL.GetSourceState(sourceID);
            }
        }

		/// <summary>
		/// Starts/Changes the song
		/// </summary>
		/// <param name="filename">defaults to Content/ folder</param>
		public static void PlaySong(string filename, bool repeat = true, bool fadeIn = false, int fadeTime = 120)
		{
			SoundEffect song = ContentPipe.LoadSoundEffect(filename);

			if (sourceID != -1)
			{
				ALSourceState state = AL.GetSourceState(sourceID);
				if (state == ALSourceState.Playing)
				{
					AL.SourceStop(sourceID);
				}
			}
			else
			{
				sourceID = AL.GenSource();
			}

			AL.Source(sourceID, ALSourcei.Buffer, song.BufferId);
			AL.Source(sourceID, ALSourcef.Gain, volume);
			AL.Source(sourceID, ALSourceb.Looping, repeat);

            AL.SourcePlay(sourceID);

            if (fadeIn)
			{
				FadeIn(fadeTime);
			}
		}

		public static void Update()
		{
			//if (sourceID != -1 && repeatMusic)
			//{
			//	ALSourceState state = AL.GetSourceState(sourceID);
			//	if (state == ALSourceState.Playing)
			//	{
			//		AL.SourceRewind(sourceID);
			//		AL.SourcePlay(sourceID);
			//	}
			//}

			if (fading)
			{
				fadeCurrent++;
				if (fadeCurrent > fadeTime)
				{
					StopFade();
					if (!fadingIn)
						StopMusic();
				}
			}

            if (sourceID != -1)
            {
                AL.Source(sourceID, ALSourcef.Gain, volume * Fade);
            }
        }

		public static void FadeIn(int fadeSteps)
		{
			fadeTime = fadeSteps;
			fadeCurrent = 0;
			fading = true;
			fadingIn = true;
		}
		public static void FadeOut(int fadeSteps)
		{
			fadeTime = fadeSteps;
			fadeCurrent = 0;
			fading = true;
			fadingIn = false;
		}
		public static void StopFade()
		{
			fading = false;
			fadeTime = 0;
			fadeCurrent = 0;
		}

		public static void PauseMusic()
		{
			if (sourceID != -1)
			{
				AL.SourcePause(sourceID);
			}
		}
		public static void PlayMusic()
		{
			if (sourceID != -1)
			{
				AL.SourcePlay(sourceID);
			}
		}
		public static void StopMusic()
		{
			if (sourceID != -1)
			{
				AL.SourceStop(sourceID);
			}
		}

		public static void Dispose()
		{
			Volume = 0;
			StopMusic();
		}
	}
}
