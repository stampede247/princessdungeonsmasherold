﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{
    public enum EntityType
    {
        Player,
        Box,
        Teleport1,
        Teleport2,
        Teleport3,
        Gem,
        Exit,
    }

    [Serializable]
    class EntitySerializable
    {
        private EntityType type;
        private Point position;

        public EntityType Type { get { return type; } }
        public Point Position { get { return position; } }

        public EntitySerializable(EntityType type, Point position)
        {
            this.type = type;
            this.position = position;
        }
    }
}
