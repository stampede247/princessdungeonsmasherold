﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;

namespace PrincessDungeonSmasher
{
    [Serializable]
    struct Level
    {
        private Tile[,] tiles;
        private EntitySerializable[] entities;
        private string filePath;
        private string name;
        private string author;
        public int playerStartX, playerStartY;

        public Tile this[int x, int y] {
            get
            {
                return tiles[x, y];
            }
            set
            {
                this.tiles[x, y] = value;
            }
        }
        public string FilePath { get { return filePath; } }
        public string Name { get { return name; } }
        public string Author { get { return author; } }
        public int Width { get { return tiles.GetLength(0); } }
        public int Height { get { return tiles.GetLength(1); } }
        public EntitySerializable[] Entities { get { return entities; } }

        public Level(string filePath)
        {
            Level loadedLevel = Level.LoadFromFile(filePath);

            this.tiles = new Tile[loadedLevel.Width, loadedLevel.Height];
            for (int x = 0; x < loadedLevel.Width; x++)
            {
                for (int y = 0; y < loadedLevel.Height; y++)
                {
                    this.tiles[x, y] = loadedLevel[x, y];
                }
            }

            this.author = loadedLevel.Author;
            this.name = loadedLevel.Name;
            this.filePath = filePath;
            this.playerStartX = loadedLevel.playerStartX;
            this.playerStartY = loadedLevel.playerStartY;
            this.entities = new EntitySerializable[loadedLevel.entities.Length];
            loadedLevel.entities.CopyTo(this.entities, 0);
        }
        public Level(int width, int height)
        {
            tiles = new Tile[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    tiles[x, y] = new Tile(TileType.Ground);
                }
            }
            name = "Empty Level";
            author = "";
            filePath = "";
            playerStartX = 1;
            playerStartY = 1;
            entities = new EntitySerializable[] { new EntitySerializable(EntityType.Exit, new Point(10, 10)) };
        }

        public static Level LoadFromFile(string filePath)
        {
            Level newLevel;
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File doesn't exist: {0}", filePath);
                return new Level(20, 20);
            }

            try
            {
                using (Stream stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    newLevel = (Level)formatter.Deserialize(stream);
                    return newLevel;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured while loading file from: {0}", filePath);
                Console.WriteLine("Error: {0}", e.Message);
                return new Level(20, 20);
            }
        }

        public static void SaveLevelTo(Level level, string filePath)
        {
            try
            {
                using (Stream stream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, level);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured while saving file to: {0}", filePath);
                Console.WriteLine("Error: {0}", e.Message);
            }
        }
    }
}
