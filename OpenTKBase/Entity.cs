﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{

    class Entity
    {
        public static Texture2D EntitySpriteSheet;

        private Point position, lastPosition;
        private float movePercent;
        private Point spriteGridPos;
        private bool solid, pushable;
        private bool alive;

        //How many frames it takes to move from one block to another
        protected int moveSteps;

        public Point Position { get { return position; } }
        public Point LastPosition { get { return lastPosition; } }
        public Point SpriteGridPos { get { return spriteGridPos; } }
        public bool Pushable { get { return pushable; } }
        public bool Solid { get { return solid; } }
        /// <summary>
        /// A value between 0 and 1 that represents the entities progress in moving from lastPosition to postiion
        /// </summary>
        public float MovePercent { get { return movePercent; } }

        public Entity(EntitySerializable serializable)
        {
            this.position = serializable.Position;
            this.lastPosition = serializable.Position;
            this.alive = true;
            this.movePercent = 1;
            this.moveSteps = 10;

            switch (serializable.Type)
            {
                case EntityType.Player:
                    this.spriteGridPos = new Point(0, 0);//doesn't matter what this is
                    this.solid = true;
                    this.pushable = true;
                    break;
                case EntityType.Box:
                    this.spriteGridPos = new Point(0, 0);
                    this.solid = true;
                    this.pushable = true;
                    break;
                case EntityType.Exit:
                    this.spriteGridPos = new Point(6, 0);
                    this.solid = false;
                    this.pushable = false;
                    break;
                case EntityType.Gem:
                    this.spriteGridPos = new Point(3, 0);
                    this.solid = false;
                    this.pushable = false;
                    break;
                case EntityType.Teleport1:
                    this.spriteGridPos = new Point(0, 1);
                    this.solid = false;
                    this.pushable = false;
                    break;
                case EntityType.Teleport2:
                    this.spriteGridPos = new Point(1, 1);
                    this.solid = false;
                    this.pushable = false;
                    break;
                case EntityType.Teleport3:
                    this.spriteGridPos = new Point(2, 1);
                    this.solid = false;
                    this.pushable = false;
                    break;
            }
        }

        public virtual void Update()
        {
            if (movePercent < 1)
            {
                movePercent += 1f / moveSteps;
                if (movePercent >= 1)
                {
                    movePercent = 1;
                    EndMove();
                }
            }
        }

        public virtual void EndMove()
        {

        }

        public virtual bool CanMove(Direction direction)
        {
            if (!pushable)
                return false;

            return true;
        }
        public virtual void Move(Direction direction)
        {
            this.lastPosition = position;
            this.movePercent = 0;
            switch (direction)
            {
                case Direction.Up:
                    position = new Point(position.X, position.Y - 1);
                    break;
                case Direction.Down:
                    position = new Point(position.X, position.Y + 1);
                    break;
                case Direction.Left:
                    position = new Point(position.X - 1, position.Y);
                    break;
                case Direction.Right:
                    position = new Point(position.X + 1, position.Y);
                    break;
            }
        }

        public virtual void Draw(Vector2 position, Vector2 size)
        {
            Vector2 sourcePos = new Vector2(this.spriteGridPos.X * 16, this.spriteGridPos.Y * 16);
            Spritebatch.Draw(EntitySpriteSheet, position, size, 0f, Color.White, Vector2.Zero, 0f,
                new RectangleF(sourcePos.X, sourcePos.Y, 16, 16));
        }
    }
}
