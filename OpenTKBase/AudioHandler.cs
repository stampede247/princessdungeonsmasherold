﻿using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// Creates an Audio Context and handles playing SoundEffect files.
    /// Also can act as a Loading manager if you use string overload of PlaySoundEffect or Preload functions.
    /// </summary>
	class AudioHandler
	{
		public static AudioContext context;
        /// <summary>
        /// Volume used when playing new sound effects. Will not change currently playing Sound Effects
        /// </summary>
		public static float volume = 0.8f;

        /// <summary>
        /// List of previously loaded Sound Effects. 
        /// </summary>
		private static Dictionary<string, SoundEffect> loadedSEs;

        /// <summary>
        /// Called from Game1's constructor. Creates the AudioContext
        /// </summary>
		public static void Initialize()
		{
			context = new AudioContext();

			loadedSEs = new Dictionary<string, SoundEffect>();
		}

        /// <summary>
        /// Preloads a file into the loadedSEs list so when played later it doesn't have to load it again
        /// </summary>
        /// <param name="filename"></param>
		public static void Preload(string filename)
		{
			if (!loadedSEs.ContainsKey(filename))
			{
				SoundEffect sound = ContentPipe.LoadSoundEffect(filename);
				loadedSEs.Add(filename, sound);
			}
		}

		/// <summary>
		/// Loads a sound effect from file or finds it in previously loaded files and then plays it
		/// </summary>
		/// <param name="filename">defaults to Content/ folder</param>
		/// <param name="volume">0 - 1</param>
		public static void PlaySoundEffect(string filename)
		{
			SoundEffect sound;
			if (loadedSEs.ContainsKey(filename))
			{
				sound = loadedSEs[filename];
			}
			else
            {
                sound = ContentPipe.LoadSoundEffect(filename);
                loadedSEs.Add(filename, sound);
			}

			PlaySoundEffect(sound);
		}

		/// <summary>
		/// Plays the sound effect as a new instance
		/// </summary>
		public static void PlaySoundEffect(SoundEffect effect)
		{
			AL.Source(effect.SourceId, ALSourcef.Gain, volume);
            AL.SourcePlay(effect.SourceId);
        }

        /// <summary>
        /// Deletes all loaded data and the audio context
        /// </summary>
		public static void Dispose()
		{
			context.Dispose();
			for (int i = 0; i < loadedSEs.Count; i++)
			{
				AL.DeleteBuffer(loadedSEs.ElementAt(i).Value.BufferId);
				AL.DeleteSource(loadedSEs.ElementAt(i).Value.SourceId);
			}
		}
	}
}
