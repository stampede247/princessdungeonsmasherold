﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrincessDungeonSmasher
{
    class GridSpace
    {
        public Tile type;

        public bool IsSolid { get { return type.IsSolid; } }
        public Point SpriteGridPos { get { return new Point(type.SpriteSheetX, type.SpriteSheetY); } }
        public Vector2 SpriteGridVec { get { return new Vector2(type.SpriteSheetX, type.SpriteSheetY); } }

        public GridSpace(Tile type)
        {
            this.type = type;
        }

        public void Draw(Texture2D spriteSheet, Vector2 position, Vector2 size)
        {
            Spritebatch.Draw(spriteSheet, position, size, 0, Color.White, Vector2.Zero, 0, 
                new RectangleF(this.type.SpriteSheetX * (spriteSheet.Width / 10f), this.type.SpriteSheetY * (spriteSheet.Height / 10f),
                spriteSheet.Width / 10f, spriteSheet.Height / 10f));
        }
    }
}
