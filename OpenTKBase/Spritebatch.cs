﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

//This file is not fully commented and may have bugs still as well

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// This class handles setting OpenGL's variables the way I prefer for 2D rendering and provides
    /// many functions for drawing simple primitives as well as sprites.
    /// </summary>
    class Spritebatch
    {
        private static GameWindow window;

        private static Size ScreenSize
        {
            get
            {
                return window.ClientSize;
            }
        }
        private static float minDepth, maxDepth;
        private static DepthFunction dFunc;

        public static float MinDepth
        {
            set
            {
                minDepth = value + 0.9f;
                EnableDepth();
            }
        }
        public static float MaxDepth
        {
            set
            {
                maxDepth = value + 1.1f;
                EnableDepth();
            }
        }
        public static DepthFunction DepthFunction
        {
            set
            {
                dFunc = value;
                EnableDepth();
            }
        }


        public static void Initialize(GameWindow gameWindow)
        {
            minDepth = 0.09f;
            maxDepth = 1.1f;
            dFunc = DepthFunction.Lequal;

            //Enable Textures and alpha blending
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            //Set how the alpha blending function works
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.LineSmooth);
            GL.Enable(EnableCap.LineStipple);
            EnableDepth();

            window = gameWindow;

        }

        private static void EnableDepth()
        {
            GL.Enable(EnableCap.DepthTest);
            GL.DepthRange(minDepth, maxDepth);
        }

        /// <summary>
        /// Call before spritebatch.Begin
        /// </summary>
        public static void Clear(Color color)
        {
            GL.ClearColor(color);
            GL.ClearDepth(maxDepth);
            GL.DepthFunc(dFunc);
            GL.DepthMask(true);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }
        /// <summary>
        /// Call before spritebatch.Begin
        /// Clears the screen bits to black
        /// </summary>
        public static void Clear()
        {
            GL.ClearColor(Color.Black);
            GL.ClearDepth(maxDepth);
            GL.DepthFunc(dFunc);
            GL.DepthMask(true);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        public static void Begin(View view = null)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-window.ClientSize.Width / 2f, window.ClientSize.Width / 2f, window.ClientSize.Height / 2f, -window.ClientSize.Height / 2f, minDepth, maxDepth);
            //Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, window.ClientSize.Width / (float)window.ClientSize.Height, 0.1f, 1f);
            //GL.MultMatrix(ref perspective);

            if (view != null)
            {
                view.ApplyTransform();
            }

        }

        public static void End()
        {
            //Doesn't do anything at the moment. window.SwapBuffers() is done externally now
        }

        #region Real Draw Functions
        //This region contains all the draw functions that actually call GL

        private static void DrawReal(Texture2D Texture, Vector2 position, Vector2 size, float rotation, Color color, Vector2 origin, float depth = 0f, RectangleF? sourceRec = null)
        {
            GL.BindTexture(TextureTarget.Texture2D, Texture.Id);

            GL.Begin(PrimitiveType.Quads);

            Vector2[] p = new Vector2[4] { Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero };
            p[0] = new Vector2(0, 0);
            p[1] = new Vector2(1, 0);
            p[2] = new Vector2(1, 1);
            p[3] = new Vector2(0, 1);

            Vector2[] tC = new Vector2[4] { Vector2.Zero, Vector2.Zero, Vector2.Zero, Vector2.Zero }; ;
            p.CopyTo(tC, 0);
            #region Source Rec
            if (sourceRec.HasValue)
            {
                tC[0] = new Vector2(sourceRec.Value.Left, sourceRec.Value.Top);
                tC[1] = new Vector2(sourceRec.Value.Right, sourceRec.Value.Top);
                tC[2] = new Vector2(sourceRec.Value.Right, sourceRec.Value.Bottom);
                tC[3] = new Vector2(sourceRec.Value.Left, sourceRec.Value.Bottom);
                for (int i = 0; i < 4; i++)
                {
                    tC[i] = new Vector2(tC[i].X / Texture.Width, tC[i].Y / Texture.Height);
                }
            }
            #endregion
            #region Animation
            if (Texture.IsAnimation)
            {
                RectangleF cellRec = Texture.CurrentCellRec;
                if (sourceRec.HasValue)
                {
                    tC[0] = new Vector2(cellRec.Left + sourceRec.Value.Left, cellRec.Top + sourceRec.Value.Top);
                    tC[1] = new Vector2(cellRec.Left + sourceRec.Value.Right, cellRec.Top + sourceRec.Value.Top);
                    tC[2] = new Vector2(cellRec.Left + sourceRec.Value.Right, cellRec.Top + sourceRec.Value.Bottom);
                    tC[3] = new Vector2(cellRec.Left + sourceRec.Value.Left, cellRec.Top + sourceRec.Value.Bottom);
                }
                else
                {
                    tC[0] = new Vector2(cellRec.Left, cellRec.Top);
                    tC[1] = new Vector2(cellRec.Right, cellRec.Top);
                    tC[2] = new Vector2(cellRec.Right, cellRec.Bottom);
                    tC[3] = new Vector2(cellRec.Left, cellRec.Bottom);
                }

                for (int i = 0; i < 4; i++)
                {
                    tC[i] = new Vector2(tC[i].X / Texture.Width, tC[i].Y / Texture.Height);
                }
            }
            #endregion

            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            for (int i = 0; i < 4; i++)
            {
                p[i] = new Vector2(p[i].X * size.X + position.X, p[i].Y * size.Y + position.Y);
                p[i] -= origin;
                if (rotation != 0)
                {
                    p[i] = Calc.RotateAround(p[i], position, rotation);
                }
                //p[i] = p[i] + new Vector2(ScreenSize.Width / 2f, ScreenSize.Height / 2f);
                //p[i] = new Vector2(p[i].X / (ScreenSize.Width / 2f), p[i].Y / (ScreenSize.Height / 2f));


                GL.Color4(color);
                GL.TexCoord2(tC[i]);
                //GL.Vertex2(p[i]);
                GL.Vertex3(new Vector3(p[i].X, p[i].Y, -depth - 0.1f));
            }

            GL.End();
        }

        private static void DrawLineReal(Vector2 p1, Vector2 p2, Color color1, Color color2, float thickness = 1f, float depth = 0f, short stipplePattern = 0, int stippleFactor = 1)
        {
            GL.BindTexture(TextureTarget.Texture2D, My.dot.Id);
            GL.LineWidth(thickness);
            if (stipplePattern != 0)
            {
                GL.Enable(EnableCap.LineStipple);
                GL.LineStipple(stippleFactor, stipplePattern);
            }
            else
            {
                GL.Disable(EnableCap.LineStipple);
            }
            GL.Begin(PrimitiveType.Lines);

            GL.TexCoord2(Vector2.Zero);


            GL.Color4(color1);
            GL.Vertex3(new Vector3(p1.X, p1.Y, -depth - 0.1f));
            GL.Color4(color2);
            GL.Vertex3(new Vector3(p2.X, p2.Y, -depth - 0.1f));

            GL.End();

            //Old method
            //DrawRectangle(p1, new Vector2(Calc.Distance(p1, p2), thickness), color, (float)Math.Atan2(p2.Y - p1.Y, p2.X - p1.X), new Vector2(0, thickness / 2f), depth);
        }

        private static void DrawTriangleReal(Vector2 p1, Vector2 p2, Vector2 p3, Color[] colors, float depth = 0f, Texture2D? texture = null, Triangle? sourceTriangle = null)
        {
            Vector2[] texCoords = new Vector2[3]
            {
                new Vector2(0,0),
                new Vector2(1,0),
                new Vector2(0,1),
            };
            if (sourceTriangle != null && texture.HasValue)
            {
                texCoords = new Vector2[3]
                {
                    sourceTriangle.Value.v1,
                    sourceTriangle.Value.v2,
                    sourceTriangle.Value.v3,
                };
            }
            Vector2[] verts = new Vector2[3]
            {
                p1, p2, p3
            };

            GL.BindTexture(TextureTarget.Texture2D, (texture.HasValue ? texture.Value.Id : My.dot.Id));
            GL.Begin(PrimitiveType.Triangles);
            for (int i = 0; i < 3; i++)
            {
                GL.Color4(colors[i]);
                if (texture.HasValue && sourceTriangle.HasValue)
                {
                    GL.TexCoord2(new Vector2(texCoords[i].X / texture.Value.Width, texCoords[i].Y / texture.Value.Height));
                }
                else
                {
                    GL.TexCoord2(new Vector2(texCoords[i].X, texCoords[i].Y));
                }
                GL.Vertex3(new Vector3(verts[i].X, verts[i].Y, -depth - 0.1f));
            }
            GL.End();
        }

        private static void DrawQuadReal(Quad quad, Color[] colors, float depth = 0f, Texture2D? texture = null, Quad? sourceQuad = null)
        {
            if (colors.Length != 4)
                throw new ArgumentException("Color array was not 4 nodes long");

            if (quad.IsConvex)
            {
                Quad source = new Quad(new Rectangle(0, 0, 1, 1));
                if (sourceQuad.HasValue)
                {
                    source = sourceQuad.Value;
                }
                else if (texture.HasValue)
                {
                    source = new Quad(new Rectangle(0, 0, texture.Value.Width, texture.Value.Height));
                }

                if (texture.HasValue)
                    GL.BindTexture(TextureTarget.Texture2D, texture.Value.Id);
                else
                    GL.BindTexture(TextureTarget.Texture2D, My.dot.Id);

                GL.Begin(PrimitiveType.Quads);

                for (int i = 0; i < 4; i++)
                {
                    GL.Color4(colors[i]);
                    Vector2 texCoord = source.GetVertex(i);
                    if (texture.HasValue)
                    {
                        texCoord = new Vector2(texCoord.X / texture.Value.Width, texCoord.Y / texture.Value.Height);
                    }
                    GL.TexCoord2(texCoord);
                    GL.Vertex3(new Vector3(quad.GetVertex(i).X, quad.GetVertex(i).Y, -depth - 0.1f));
                }
                GL.End();
            }
            else
            {
                Polygon sourcePoly = (Polygon)quad;
                if (sourceQuad.HasValue)
                    sourcePoly = (Polygon)sourceQuad.Value;
                DrawPolygonReal((Polygon)quad, colors, depth, texture, sourcePoly);
            }




            //if (quad.IsConvex && !quad.IsComplex)
            //{
            //    Triangle sourceTriangle1 = new Triangle(quad.v1, quad.v2, quad.v4);
            //    Triangle sourceTriangle2 = new Triangle(quad.v2, quad.v3, quad.v4);
            //    if (sourceQuad != null)
            //    {
            //        sourceTriangle1 = new Triangle(sourceQuad.Value.v2, sourceQuad.Value.v3, sourceQuad.Value.v4);
            //    }
            //    DrawTriangleReal(quad.v1, quad.v2, quad.v4, new Color[3] { colors[0], colors[1], colors[3] }, depth, texture, sourceTriangle1);
            //    DrawTriangleReal(quad.v2, quad.v3, quad.v4, new Color[3] { colors[1], colors[2], colors[3] }, depth, texture, sourceTriangle2);
            //}
            //else
            //{
            //    Polygon? sourcePoly = null;
            //    if (sourceQuad.HasValue)
            //        sourcePoly = (Polygon)sourceQuad.Value;
            //    DrawPolygonReal((Polygon)quad, colors, depth, texture, sourcePoly);
            //}
        }

        /// <summary>
        /// This one kind of isn't a 'Real' function since it passes
        /// to DrawTriangleReal to do it's stuff
        /// </summary>
        private static void DrawPolygonReal(Polygon poly, Color[] colors, float depth = 0f, Texture2D? texture = null, Polygon? sourcePoly = null)
        {
            Color avgColor;
            #region Calc Average color for center vertex
            {
                int avgR = 0, avgG = 0, avgB = 0, avgA = 0;
                for (int i = 0; i < colors.Length; i++)
                {
                    avgR += colors[i].R;
                    avgG += colors[i].G;
                    avgB += colors[i].B;
                    avgA += colors[i].A;
                }
                avgR /= colors.Length;
                avgG /= colors.Length;
                avgB /= colors.Length;
                avgA /= colors.Length;

                avgColor = Color.FromArgb(avgA, avgR, avgG, avgB);
            }
            #endregion

            for (int i = 0; i < poly.NumVerts; i++)
            {
                int i2 = (i + 1) % poly.NumVerts;
                Triangle triangle = new Triangle(poly.verts[0], poly.verts[i], poly.verts[i2]);
                Triangle sourceTriangle = new Triangle(Vector2.Zero, Vector2.Zero, Vector2.Zero);
                if (sourcePoly.HasValue)
                {
                    sourceTriangle = new Triangle(sourcePoly.Value.Center, sourcePoly.Value.verts[i], sourcePoly.Value.verts[i2]);
                }



                DrawTriangleReal(triangle.v1, triangle.v2, triangle.v3, new Color[3] { avgColor, colors[i], colors[i2] }, depth, texture, sourceTriangle);
            }
        }
        #endregion

        #region Sprite Draw Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation">In Radians</param>
        public static void Draw(Texture2D Texture, Vector2 position, Vector2 size)
        {
            DrawReal(Texture, position, size, 0f, Color.White, Vector2.Zero, 0f);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation">In Radians</param>
        public static void Draw(Texture2D Texture, Vector2 position, Vector2 size, float rotation)
        {
            DrawReal(Texture, position, size, rotation, Color.White, Vector2.Zero, 0f);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation">In Radians</param>
        public static void Draw(Texture2D Texture, Vector2 position, Vector2 size, float rotation, Color color)
        {
            DrawReal(Texture, position, size, rotation, color, Vector2.Zero, 0f);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation">In Radians</param>
        public static void Draw(Texture2D Texture, Vector2 position, Vector2 size, float rotation, Color color, Vector2 origin)
        {
            DrawReal(Texture, position, size, rotation, color, origin, 0f);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation">In Radians</param>
        public static void Draw(Texture2D Texture, Vector2 position, Vector2 size, float rotation, Color color, Vector2 origin, float depth = 0f, RectangleF? sourceRec = null)
        {
            DrawReal(Texture, position, size, rotation, color, origin, depth, sourceRec);
        }
        #endregion

        #region Rectangle functions
        public static void DrawRectangle(Rectangle rec, Color color, float depth = 0f)
        {
            DrawRectangle(new Vector2(rec.X, rec.Y), new Vector2(rec.Width, rec.Height), color, 0f, Vector2.Zero, depth);
        }
        public static void DrawRectangle(RectangleF rec, Color color, float depth = 0f)
        {
            DrawRectangle(new Vector2(rec.X, rec.Y), new Vector2(rec.Width, rec.Height), color, 0f, Vector2.Zero, depth);
        }
        public static void DrawRectangle(OBB rec, Color color, float depth = 0f)
        {
            DrawRectangle(rec.center, rec.size, color, rec.rotationF, rec.HalfSize, depth);
        }
        public static void DrawRectangle(Vector2 position, Vector2 size, Color color)
        {
            DrawRectangle(position.X, position.Y, size.X, size.Y, color, 0, Vector2.Zero, 0);
        }
        public static void DrawRectangle(Vector2 position, Vector2 size, Color color, float rotation)
        {
            DrawRectangle(position.X, position.Y, size.X, size.Y, color, rotation, Vector2.Zero, 0);
        }
        public static void DrawRectangle(Vector2 position, Vector2 size, Color color, float rotation, Vector2 origin)
        {
            DrawRectangle(position.X, position.Y, size.X, size.Y, color, rotation, origin, 0);
        }
        public static void DrawRectangle(Vector2 position, Vector2 size, Color color, float rotation, Vector2 origin, float depth)
        {
            DrawRectangle(position.X, position.Y, size.X, size.Y, color, rotation, origin, depth);
        }

        public static void DrawRectangle(float x, float y, float width, float height, Color color)
        {
            Draw(My.dot, new Vector2(x, y), new Vector2(width, height), 0, color, Vector2.Zero, 0);
        }
        public static void DrawRectangle(float x, float y, float width, float height, Color color, float rotation)
        {
            Draw(My.dot, new Vector2(x, y), new Vector2(width, height), rotation, color, Vector2.Zero, 0);
        }
        public static void DrawRectangle(float x, float y, float width, float height, Color color, float rotation, Vector2 origin)
        {
            Draw(My.dot, new Vector2(x, y), new Vector2(width, height), rotation, color, origin, 0);
        }
        public static void DrawRectangle(float x, float y, float width, float height, Color color, float rotation, Vector2 origin, float depth)
        {
            Draw(My.dot, new Vector2(x, y), new Vector2(width, height), rotation, color, origin, depth);
        }
        #endregion

        #region Triangle Functions
        public static void DrawTriangle(Triangle triangle, Color color, float depth = 0f, Texture2D? texture = null, Triangle? sourceTriangle = null)
        {
            DrawTriangleReal(triangle.v1, triangle.v2, triangle.v3, new Color[3] { color, color, color }, depth, texture, sourceTriangle);
        }
        public static void DrawTriangle(Triangle triangle, Color[] colors, float depth = 0f, Texture2D? texture = null, Triangle? sourceTriangle = null)
        {
            DrawTriangleReal(triangle.v1, triangle.v2, triangle.v3, colors, depth, texture, sourceTriangle);
        }
        public static void DrawTriangle(Vector2 p1, Vector2 p2, Vector2 p3, Color color, float depth = 0f, Texture2D? texture = null, Triangle? sourceTriangle = null)
        {
            DrawTriangleReal(p1, p2, p3, new Color[3] { color, color, color }, depth, texture, sourceTriangle);
        }
        public static void DrawTriangle(Vector2 p1, Vector2 p2, Vector2 p3, Color[] colors, float depth = 0f, Texture2D? texture = null, Triangle? sourceTriangle = null)
        {
            DrawTriangleReal(p1, p2, p3, colors, depth, texture, sourceTriangle);
        }
        #endregion

        #region Quad Functions
        public static void DrawQuad(Quad quad, Color color, float depth = 0f, Texture2D? texture = null, Quad? sourceQuad = null)
        {
            DrawQuadReal(quad, new Color[4] { color, color, color, color }, depth, texture, sourceQuad);
        }
        public static void DrawQuad(Quad quad, Color[] colors, float depth = 0f, Texture2D? texture = null, Quad? sourceQuad = null)
        {
            DrawQuadReal(quad, colors, depth, texture, sourceQuad);
        }
        public static void DrawQuad(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Color color, float depth = 0f, Texture2D? texture = null, Quad? sourceQuad = null)
        {
            DrawQuadReal(new Quad(p1, p2, p3, p4), new Color[4] { color, color, color, color }, depth, texture, sourceQuad);
        }
        public static void DrawQuad(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Color[] colors, float depth = 0f, Texture2D? texture = null, Quad? sourceQuad = null)
        {
            DrawQuadReal(new Quad(p1, p2, p3, p4), colors, depth, texture, sourceQuad);
        }
        #endregion

        #region Polygon Functions
        /// <summary>
        /// This only works for convex polygons!
        /// </summary>
        public static void DrawPolygon(Vector2[] verts, Color color, float depth = 0f, Texture2D? texture = null, Polygon? sourcePoly = null)
        {
            Color[] colors = new Color[verts.Length];
            for (int i = 0; i < verts.Length; i++)
                colors[i] = color;
            DrawPolygon(new Polygon(verts), colors, depth, texture, sourcePoly);
        }
        /// <summary>
        /// This only works for convex polygons!
        /// </summary>
        public static void DrawPolygon(Vector2[] verts, Color[] colors, float depth = 0f, Texture2D? texture = null, Polygon? sourcePoly = null)
        {
            DrawPolygon(new Polygon(verts), colors, depth, texture, sourcePoly);
        }
        /// <summary>
        /// This only works for convex polygons!
        /// </summary>
        public static void DrawPolygon(Polygon poly, Color color, float depth = 0f, Texture2D? texture = null, Polygon? sourcePoly = null)
        {
            Color[] colors = new Color[poly.NumVerts];
            for (int i = 0; i < poly.NumVerts; i++)
                colors[i] = color;
            DrawPolygon(poly, colors, depth, texture, sourcePoly);
        }
        /// <summary>
        /// This only works for convex polygons!
        /// </summary>
        public static void DrawPolygon(Polygon poly, Color[] colors, float depth = 0f, Texture2D? texture = null, Polygon? sourcePoly = null)
        {
            if (sourcePoly.HasValue && poly.NumVerts != sourcePoly.Value.NumVerts)
            {
                throw new ArgumentException("The source poly did not have the same number of vertices as the polygon we are drawing");
            }
            if (poly.NumVerts != colors.Length)
            {
                throw new ArgumentException("The colors array did not have the same number of points as the polygon");
            }

            if (poly.NumVerts == 1)
            {
                DrawPoint(poly.verts[0], 15f, colors[0], depth);
            }
            else if (poly.NumVerts == 2)
            {
                DrawLineReal(poly.verts[0], poly.verts[1], colors[0], colors[1], 1f, depth);
            }
            else if (poly.NumVerts == 3)
            {
                if (sourcePoly.HasValue)
                {
                    DrawTriangleReal(poly.verts[0], poly.verts[1], poly.verts[2], new Color[3] { colors[0], colors[1], colors[2] },
                        depth, texture, new Triangle(sourcePoly.Value.verts[0], sourcePoly.Value.verts[1], sourcePoly.Value.verts[2]));
                }
                else
                {
                    DrawTriangle(poly.verts[0], poly.verts[1], poly.verts[2], new Color[3] { colors[0], colors[1], colors[2] },
                        depth, texture, null);
                }
            }
            else
            {
                Color avgColor;
                {
                    int avgR = 0, avgG = 0, avgB = 0, avgA = 0;
                    for (int i = 0; i < colors.Length; i++)
                    {
                        avgR += colors[i].R;
                        avgG += colors[i].G;
                        avgB += colors[i].B;
                        avgA += colors[i].A;
                    }
                    avgR /= colors.Length;
                    avgG /= colors.Length;
                    avgB /= colors.Length;
                    avgA /= colors.Length;

                    avgColor = Color.FromArgb(avgA, avgR, avgG, avgB);
                }

                for (int i = 0; i < poly.NumVerts; i++)
                {
                    int i2 = (i + 1) % poly.NumVerts;
                    Triangle triangle = new Triangle(poly.verts[0], poly.verts[i], poly.verts[i2]);
                    Triangle sourceTriangle = new Triangle(Vector2.Zero, Vector2.Zero, Vector2.Zero);
                    if (sourcePoly.HasValue)
                    {
                        sourceTriangle = new Triangle(sourcePoly.Value.verts[0], sourcePoly.Value.verts[i], sourcePoly.Value.verts[i2]);
                    }



                    DrawTriangle(triangle, new Color[3] {  colors[0], colors[i], colors[i2] }, depth, texture, sourceTriangle);
                }
            }
        }
        #endregion

        #region Circle Functions
        public static void DrawCircle(Vector2 center, float radius, Color color, int numSegments, float depth = 0f, Texture2D? texture = null, Circle? sourceCircle = null)
        {
            DrawCircle(new Circle(center, radius), color, numSegments, depth, texture, sourceCircle);
        }
        public static void DrawCircle(Circle circle, Color color, int numSegments, float depth = 0f, Texture2D? texture = null, Circle? sourceCircle = null)
        {
            float pieceSize = MathHelper.Pi * 2f / numSegments;
            for (int i = 0; i < numSegments; i++)
            {
                Vector2 p1 = Calc.CirclePoint(i * pieceSize, circle.radius) + circle.center;
                Vector2 p2 = Calc.CirclePoint((i + 1) * pieceSize, circle.radius) + circle.center;

                if (texture != null)
                {
                    Vector2 sP1 = p1;
                    Vector2 sP2 = p2;
                    Vector2 spC = circle.center;
                    if (sourceCircle != null)
                    {
                        sP1 = Calc.CirclePoint(i * pieceSize, sourceCircle.Value.radius) + sourceCircle.Value.center;
                        sP2 = Calc.CirclePoint((i + 1) * pieceSize, sourceCircle.Value.radius) + sourceCircle.Value.center;
                        spC = sourceCircle.Value.center;
                    }

                    DrawTriangleReal(circle.center, p1, p2, new Color[3] { color, color, color }, depth, texture, new Triangle(spC, sP1, sP2));
                }
                else
                {
                    DrawTriangleReal(circle.center, p1, p2, new Color[3] { color, color, color }, depth);
                }
            }
        }
        #endregion

        #region Line Functions
        public static void DrawLine(Line line, Color color, float thickness = 1f, float depth = 0f)
        {
            DrawLineReal(line.p1, line.p2, color, color, thickness, depth);
        }
        public static void DrawLine(Line line, Color color1, Color color2, float thickness = 1f, float depth = 0f)
        {
            DrawLineReal(line.p1, line.p2, color1, color2, thickness, depth);
        }
        public static void DrawLine(Vector2 p1, Vector2 p2, Color color, float thickness = 1f, float depth = 0f)
        {
            DrawLineReal(p1, p2, color, color, thickness, depth);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stippleFactor"> max value of 255</param>
        public static void DrawLine(Vector2 p1, Vector2 p2, Color color1, Color color2, float thickness = 1f, float depth = 0f, short stipplePattern = 0, int stippleFactor = 1)
        {
            DrawLineReal(p1, p2, color1, color2, thickness, depth, stipplePattern, stippleFactor);
        }
        #endregion

        #region Helper Functions
        public static void DrawArrow(Vector2 from, Vector2 to, Color color, float thickness = 1f, float arrowWidth = 10f, float depth = 0f)
        {
            float sqrRootTwo = 1.41421356237f;
            float sideLength = arrowWidth / sqrRootTwo;
            double dir = Calc.DirectionTo(to, from);
            Vector2 s1 = to + Calc.CirclePoint((float)dir + MathHelper.PiOver4, sideLength);
            Vector2 s2 = to + Calc.CirclePoint((float)dir - MathHelper.PiOver4, sideLength);
            DrawLine(from, to, color, thickness, depth);
            DrawLine(to, s1, color, thickness, depth);
            DrawLine(to, s2, color, thickness, depth);

        }
        public static void DrawPoint(Vector2 pos, float size, Color color, float depth = 0f)
        {
            DrawRectangle(pos, new Vector2(size, size), color, 0f, new Vector2(size / 2f, size / 2f), depth);
        }
        #endregion

        #region Outline functions
        public static void DrawPolygonOutline(Polygon poly, Color color, float thickness = 1f, float depth = 0f)
        {
            for (int i = 0; i < poly.NumVerts; i++)
            {
                int i2 = (i + 1) % poly.NumVerts;
                DrawLine(poly.verts[i], poly.verts[i2], color, thickness, depth);
            }
        }
        public static void DrawPolygonOutline(Polygon poly, Color[] colors, float thickness = 1f, float depth = 0f)
        {
            if (poly.NumVerts != colors.Length)
            {
                throw new ArgumentException("Color array was not the same length as the polygon count");
            }

            for (int i = 0; i < poly.NumVerts; i++)
            {
                int i2 = (i + 1) % poly.NumVerts;
                DrawLine(poly.verts[i], poly.verts[i2], colors[i], colors[i2], thickness, depth);
            }
        }

        public static void DrawCircleOutline(Circle circle, Color color, int numSegments, float thickness = 1f, float depth = 0f)
        {
            DrawCircleOutline(circle.center, circle.radius, color, numSegments, thickness, depth);
        }
        public static void DrawCircleOutline(Vector2 center, float radius, Color color, int numSegments, float thickness = 1f, float depth = 0f)
        {
            for (int i = 0; i < numSegments; i++)
            {
                float theta = (MathHelper.Pi * 2) / numSegments;
                Vector2 p1 = Calc.CirclePoint(theta * i, radius) + center;
                Vector2 p2 = Calc.CirclePoint(theta * (i + 1), radius) + center;
                DrawLine(p1, p2, color, thickness, depth);
            }
        }

        public static void DrawTriangleOutline(Triangle triangle, Color color, float thickness = 1f, float depth = 0f)
        {
            DrawTriangleOutline(triangle.v1, triangle.v2, triangle.v3, color, thickness, depth);
        }
        public static void DrawTriangleOutline(Vector2 v1, Vector2 v2, Vector2 v3, Color color, float thickness = 1f, float depth = 0f)
        {
            DrawLine(v1, v2, color, thickness, depth);
            DrawLine(v2, v3, color, thickness, depth);
            DrawLine(v3, v1, color, thickness, depth);
        }
        #endregion
    }
}
