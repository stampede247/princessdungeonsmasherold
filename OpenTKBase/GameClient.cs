﻿using OpenTK;
using System.Drawing;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// The GameClient class is where all your in game code should be done.
    /// If another game state besides "in-game" is added, you should make a different client class
    /// and work with the Game1.cs to handle switching between clients
    /// </summary>
    class GameClient
    {
        /// <summary>
        /// The view object for our game client
        /// </summary>
        public View view;
        /// <summary>
        /// A reference to the Game1 object created in Program.cs
        /// </summary>
        public Game1 game1;

        public Level loadedLevel;
        public GridSpace[,] grid;
        List<Entity> entities;
        public Texture2D tileSheet;

        /// <summary>
        /// Basic constructor, doesn't do anything besides save a reference to game1
        /// </summary>
        /// <param name="game1">A reference to the game1 object created in Program.cs</param>
        public GameClient(Game1 game1)
        {
            //Make sure you don't do anything here that
            //should be done in Initialize
            this.game1 = game1;
        }

        /// <summary>
        /// Called by Game1 before Initialize
        /// </summary>
        public void LoadContent()
        {
            tileSheet = ContentPipe.LoadTexture("tileSheet.png", true);
            Entity.EntitySpriteSheet = ContentPipe.LoadTexture("entitiesSheet.png", true);
        }
        /// <summary>
        /// This is our initialize function. It is called when a GameClient class is getting ready to run. 
        /// LoadContent is called just before.
        /// </summary>
        public void Initialize()
        {
            view = new View(game1.window, Vector2.Zero, 10, 1, 0);
            view.enableRounding = true;

            if (File.Exists("save.level"))
            {
                loadedLevel = new Level("save.level");
            }
            else
            {
                loadedLevel = new Level(20, 20);
            }
            GotoLevel(loadedLevel);
        }

        public void GotoLevel(Level newLevel)
        {
            this.loadedLevel = newLevel;
            this.grid = new GridSpace[newLevel.Width, newLevel.Height];

            for (int x = 0; x < newLevel.Width; x++)
            {
                for (int y = 0; y < newLevel.Height; y++)
                {
                    grid[x, y] = new GridSpace(newLevel[x, y]);
                }
            }

            this.entities = new List<Entity>();
            for (int i = 0; i < loadedLevel.Entities.Length; i++)
            {
                this.entities.Add(new Entity(loadedLevel.Entities[i]));
            }
        }
        
        /// <param name="elapsedMils">Milliseconds elapsed since last Update</param>
        public void Update(double elapsedMils)
        {
            if (My.KeyDown(Key.ControlLeft) && My.KeyPress(Key.S))
            {
                for (int x = 0; x < loadedLevel.Width; x++)
                {
                    for (int y = 0; y < loadedLevel.Height; y++)
                    {
                        loadedLevel[x, y] = grid[x, y].type;
                    }
                }

                Level.SaveLevelTo(loadedLevel, "save.level");
                Console.WriteLine("Saved level to save.level");
            }

            Vector2 mousePos = view.MousePos;
            if (mousePos.X >= 0 && mousePos.Y >= 0 &&
                mousePos.X < grid.GetLength(0) * 16 &&
                mousePos.Y < grid.GetLength(1) * 16)
            {
                Point mousePoint = new Point((int)(mousePos.X / 16), (int)(mousePos.Y / 16));
                if (My.MouseDown(MouseButton.Left))
                {
                    if (My.KeyDown(Key.ShiftLeft))
                    {
                        entities.Add(new Entity(new EntitySerializable(EntityType.Box, mousePoint)));
                    }
                    else
                    {
                        this.grid[mousePoint.X, mousePoint.Y] = new GridSpace(new Tile(TileType.Wall));
                    }
                }
                if (My.MouseDown(MouseButton.Right))
                {
                    if (My.KeyDown(Key.ShiftLeft))
                    {

                    }
                    else
                    {
                        this.grid[mousePoint.X, mousePoint.Y] = new GridSpace(new Tile(TileType.Lava));
                    }
                }
            }

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Update();
            }

            view.BasicMovement(5.0f, true, true, true, true);
            view.Update();
        }

        /// <param name="elapsedMils">Milliseconds elapsed since last Render</param>
        public void Draw(double elapsedMils)
        {
            Spritebatch.Clear(Color.Black);
            Spritebatch.Begin(view);

            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    this.grid[x, y].Draw(tileSheet, new Vector2(x * 16, y * 16), new Vector2(16, 16));
                }
            }

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Draw(new Vector2(entities[i].Position.X, entities[i].Position.Y) * new Vector2(16, 16), new Vector2(16, 16));
            }

            Spritebatch.End();

            //view.DrawDebug(new Vector2(0, 50), Color.White, QuickFont.QFontAlignment.Left);
        }
    }
}
