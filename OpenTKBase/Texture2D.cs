﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// Contains OpenGL texture ID of loaded texture. 
    /// Contains info about the texture and it's origin
    /// supports animation if it finds a .ani file of the same name.
    /// </summary>
    struct Texture2D
    {
        /// <summary>
        /// This number is in milliseconds since start of game
        /// </summary>
        private static double Timer;

        /// <summary>
        /// Sets the private variable Timer for updating of animations
        /// </summary>
        /// <param name="newTime">Milliseconds since start of game</param>
        public static void UpdateTimer(double newTime)
        {
            Timer = newTime;
        }

        /// <summary>
        /// The openGL ID of the texture
        /// </summary>
        private int id;
        /// <summary>
        /// Size of the entire texture
        /// </summary>
        private Vector2 size;
        /// <summary>
        /// The filename that was passed to ContentPipe. "Content/Sprites/" are inferred and not included
        /// </summary>
        private string fileName;
        /// <summary>
        /// Indicates whether the file was an animation.
        /// In order to make a compatible animation there needs to be 
        /// a .ani file of the same name in the same directory as the texture
        /// </summary>
        private bool isAnimation;
        /// <summary>
        /// The number of frames that compose the animation
        /// </summary>
        private int numFrames;
        /// <summary>
        /// Controls the time spent on each frame.
        /// Changing this without changing the Timer
        /// will change the current frame.
        /// </summary>
        private float milsPerFrame;

        /// <summary>
        /// The filename that was passed to ContentPipe. "Content/Sprites/" are inferred and not included
        /// </summary>
        public string FileName
        {
            get { return fileName; }
        }
        /// <summary>
        /// Indicates whether the file was an animation.
        /// In order to make a compatible animation there needs to be 
        /// a .ani file of the same name in the same directory as the texture
        /// </summary>
        public bool IsAnimation
        {
            get
            {
                return isAnimation;
            }
        }
        /// <summary>
        /// The number of frames that compose the animation
        /// </summary>
        public int NumFrames
        {
            get
            {
                return numFrames;
            }
        }
        /// <summary>
        /// Size of the entire texture
        /// </summary>
        public Vector2 Size
        {
            get 
            { 
                return new Vector2(size.X, size.Y); 
            }
        }
        /// <summary>
        /// The openGL ID of the texture
        /// </summary>
        public int Id
        {
            get { return id; }
        }
        /// <summary>
        /// Width of the entire texture
        /// </summary>
        public int Width
        {
            get
            {
                return (int)size.X;
            }
        }
        /// <summary>
        /// Height of the entire texture
        /// </summary>
        public int Height
        {
            get
            {
                return (int)size.Y;
            }
        }
        /// <summary>
        /// The size of an individual cell in an animation. 
        /// For non-animation textures this is the same as Size
        /// </summary>
        public Vector2 CellSize
        {
            get
            {
                return new Vector2(size.X / numFrames, size.Y);
            }
        }
        /// <summary>
        /// Width of a single cell in an animation.
        /// For non-animation textures this is the same as Width
        /// </summary>
        public float CellWidth
        {
            get
            {
                return size.X / numFrames;
            }
        }
        /// <summary>
        /// Height of a single cell in an animation.
        /// For non-animation textures this is the same as Height
        /// </summary>
        public float CellHeight
        {
            get
            {
                return size.Y;
            }
        }
        /// <summary>
        /// Controls the time spent on each frame.
        /// Changing this without changing the Timer
        /// will change the current frame.
        /// </summary>
        public float MilisecondsPerFrame
        {
            get
            {
                return milsPerFrame;
            }
            set
            {
                this.milsPerFrame = value;
            }
        }
        /// <summary>
        /// Equal to 1000 / MillisecondsPerFrame
        /// </summary>
        public float FramesPerSecond
        {
            get
            {
                return 1000f / milsPerFrame;
            }
            set
            {
                this.milsPerFrame = 1000f / value;
            }
        }
        /// <summary>
        /// Using Timer it returns the current frame of the animation.
        /// Animation is automatically updated without any input besides updating Timer
        /// </summary>
        public int CurrentFrame
        {
            get
            {
                return (int)(Timer / milsPerFrame) % numFrames;
            }
        }
        /// <summary>
        /// Gets the source rectangle for the current cell of the animation.
        /// For non-animation textures this is simply the whole image
        /// </summary>
        public RectangleF CurrentCellRec
        {
            get
            {
                return new RectangleF(CurrentFrame * CellWidth, 0, CellWidth, CellHeight);
            }
        }

        /// <summary>
        /// Constructor for regular, non-animation, Texture2D
        /// </summary>
        /// <param name="fileName">The filename passed to ContentPipe (excluding Content/Sprites/)</param>
        /// <param name="id">OpenGL texture ID</param>
        /// <param name="width">Width of the full image</param>
        /// <param name="height">Height of the fulll image</param>
        public Texture2D(string fileName, int id, int width, int height)
        {
            this.fileName = fileName;
            this.id = id;
            this.size = new Vector2(width, height);
            this.isAnimation = false;
            this.numFrames = 1;
            this.milsPerFrame = 1;
        }
        /// <summary>
        /// Constructor used for Texture2Ds that contain animation frames
        /// </summary>
        /// <param name="fileName">The filename passed to ContentPipe (excluding Content/Sprites/)</param>
        /// <param name="id">OpenGL texture ID</param>
        /// <param name="stripWidth">Width of the full image</param>
        /// <param name="stripHeight">Height of the fulll image</param>
        /// <param name="numFrames">Number of cells or "frames" in the animation</param>
        /// <param name="milsPerFrame">Sets the speed of the animation. Can be set at any time</param>
        public Texture2D(string fileName, int id, int stripWidth, int stripHeight, int numFrames, float milsPerFrame = 1000f / 20f)
        {
            this.fileName = fileName;
            this.id = id;
            this.size = new Vector2(stripWidth, stripHeight);
            this.isAnimation = true;
            this.numFrames = numFrames;
            this.milsPerFrame = milsPerFrame;
        }
    }
}
