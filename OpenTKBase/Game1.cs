﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using OpenTK.Input;
using QuickFont;
using System.Diagnostics;

namespace PrincessDungeonSmasher
{
    /// <summary>
    /// This is the base class for everything else in my game.
    /// A GameWindow is created in Program.cs and passed to an instance of Game1.
    /// Game1 holds a reference to the GameWindow and handles intializing, updating,
    /// and drawing all other default aspects of "My Engine"
    /// When created it hooks onto the GameWindow's events and handles updating and
    /// drawing whichever client is currently active.
    /// </summary>
    class Game1
    {
        //These are all referenced in the Program.cs when the GameWindow is being created
        public static double WINDOW_FRAMERATE = 60.0;
        public static int WINDOW_WIDTH = 640;
        public static int WINDOW_HEIGHT = 480;
        public static string WINDOW_NAME = "OpenTK Test";
        public static int COLOR_BITS = 32, DEPTH_BITS = 8, STENCIL_BITS = 0, SAMPLE_BITS = 8;
        public static GameWindowFlags WINDOW_FLAGS = GameWindowFlags.Default;

        public static bool DrawFPS = true, DrawTestText = false;

        public OpenTK.GameWindow window;
        public ConsoleHandler myConsole;
        Stopwatch framesTimer, stepsTimer;
        double framesPerSecond, stepsPerSecond;

        GameClient gameClient;

        /// <summary>
        /// This automatically binds Game1's function to window's events
        /// </summary>
        /// <param name="window">The GameWindow that will control this Game1</param>
        public Game1(OpenTK.GameWindow window)
        {
            //I believe this is just a reference to the window object
            this.window = window;
            window.Load += this.Load;
            window.Resize += this.Resize;
            window.RenderFrame += this.Render;
            window.UpdateFrame += this.Update;
            window.Closing += Closing;
            window.VSync = VSyncMode.On;
            My.Initialize(window);
            Spritebatch.Initialize(window);
            AudioHandler.Initialize();
            framesTimer = new Stopwatch();
            stepsTimer = new Stopwatch();
            myConsole = new ConsoleHandler(this);

            gameClient = new GameClient(this);
        }


        /// <summary>
        /// Loads content and calls Initialize for first active client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Load(object sender, EventArgs e)
        {
            gameClient.LoadContent();
            gameClient.Initialize();

        }

        /// <summary>
        /// Event Handler for window.Closing event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AudioHandler.Dispose();
            e.Cancel = false;
        }

        /// <summary>
        /// Measures the steps taken per second. 
        /// Handles any static or general classes that need to be updated. (My, Texture2D, ect.)
        /// Escape exits game. Tilde activates the ConsoleHandler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Update(object sender, EventArgs e)
        {
            double mils = stepsTimer.Elapsed.TotalMilliseconds;
            stepsPerSecond = Math.Round(1000.0 / mils, 0);
            stepsTimer.Restart();
            Texture2D.UpdateTimer(My.GameTime.Elapsed.TotalMilliseconds);

            My.Update();

            if (window.Keyboard[Key.Escape])
            {
                window.Exit();
            }
            if (My.KeyPress(Key.Tilde))
            {
                myConsole.Open();
            }

            gameClient.Update(mils);


            MusicHandler.Update();
            My.UpdateAfter();
        }

        /// <summary>
        /// Measues FPS and draws it to the screen if DrawFPS is true.
        /// Tells the current Client to draw.
        /// Checks for OpenGL errors thrown and writes them to the console
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Render(object sender, EventArgs e)
        {
            double mils = framesTimer.Elapsed.TotalMilliseconds;
            framesPerSecond = Math.Round(1000.0 / mils, 0);
            framesTimer.Restart();

            gameClient.Draw(mils);

            //Check for OpenGL Errors
            ErrorCode error = GL.GetError();
            if (error != ErrorCode.NoError)
            {
                Console.WriteLine(error);
            }

            QFont.Begin();

            #region Drawing Text
            //view.DrawDebug(font, Vector2.Zero, Color.Black);

            QFontRenderOptions op = new QFontRenderOptions();
            op.Colour = Color.White;
            My.font.PushOptions(op);

            if (DrawFPS)
            {
                string framesText = String.Format("Steps: {0}\nFrames: {1}", stepsPerSecond, framesPerSecond);
                My.font.Print(framesText, QFontAlignment.Left, new Vector2(0, 0));
            }
            if (DrawTestText)
            {
                op.Colour = Color.Red;
                My.font.PushOptions(op);
                My.font.Print("Hello World!", QFontAlignment.Left, new Vector2(0, 50));
            }

            #endregion

            QFont.End();

            window.SwapBuffers();
        }

        /// <summary>
        /// Calls GL.Viewport to resize the render frame and lets QFont know to resize as well
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Resize(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, window.Width, window.Height);
            QFont.InvalidateViewport();
        }
    }
}
